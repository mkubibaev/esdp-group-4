import axios from '../../axios';
import {NotificationManager} from "react-notifications";


export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';


export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, comments});


export const fetchComments = postId => {
    return dispatch => {
        return axios.get('/comment/' + postId).then(
            response => dispatch(fetchCommentsSuccess(response.data))
        )
    }
};


export const createComment = (commentData, id) => {
    return dispatch => {
        return axios.post('/comment', commentData).then(
            response => {
                dispatch(fetchComments(id));
                NotificationManager.success(response.data.message);
            }
        );
    };
};
export const deleteComment = (id, newsId) => {
    return (dispatch) => {
        return axios.delete(`/comment/${id}`).then(
            response => {
                dispatch(fetchComments(newsId));
                NotificationManager.warning(response.data.message);
            }
        )
    }
};



