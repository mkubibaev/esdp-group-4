import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Col, Row} from "reactstrap";

import UserCabinetMenu from "../../Components/UserCabinetMenu/UserCabinetMenu";


class UserCabinet extends Component {

    render() {
        return (
            <Fragment>
                <h5>Личный кабинет</h5>
                <Row>
                    <Col xs="12" md="3">
                        <UserCabinetMenu/>
                    </Col>
                    <Col xs="12" md="9">
                        <h5>Добро пожаловать в Ваш Личный кабинет!</h5>
                    </Col>
                </Row>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});


export default connect(mapStateToProps)(UserCabinet);
