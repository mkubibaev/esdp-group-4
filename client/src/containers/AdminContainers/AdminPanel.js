import React, {Component, Fragment} from 'react';
import {Badge, Col, Container, ListGroup, ListGroupItem, Row} from "reactstrap";
import {NavLink as RouterNavLink,} from "react-router-dom";
import AdminRoutes from "../../AdminRoutes";
import {fetchAdminNotifications} from "../../store/actions/adminActions";
import {connect} from "react-redux";


class AdminPanel extends Component {
    state = {
        number: ""
    };

    componentDidMount() {
        this.props.fetchAdminNotifications().then(
            result => {
                let notifications = result.data.filter(function (notification) {
                    return notification.status === "new"
                });
                this.setState({number: notifications.length});
            }
        )
    }

    changeNumber = () => {
        this.setState({number: ""})
    };

    render() {
        return (
            <Fragment>
                <Container className="py-2">
                    <Row>
                        <Col xs="12" md="3">
                            <ListGroup>
                                <ListGroupItem tag={RouterNavLink} to='/admin' exact>Общие данные</ListGroupItem>
                                <ListGroupItem onClick={this.changeNumber} tag={RouterNavLink} to='/admin/notifications' exact>
                                    Оповещения <Badge color="danger" pill>{this.state.number === 0 ? "" : this.state.number}</Badge>
                                </ListGroupItem>
                                <ListGroupItem tag={RouterNavLink} to='/admin/doctors'>Список врачебных
                                    аккаутов</ListGroupItem>
                                <ListGroupItem tag={RouterNavLink} to='/admin/clinics'>Список клиник</ListGroupItem>
                                <ListGroupItem tag={RouterNavLink} to='/admin/doctor_category'>Список категорий
                                    врачей</ListGroupItem>
                            </ListGroup>
                        </Col>
                        <Col xs="12" md="9">
                            <AdminRoutes
                                user={this.props.user}
                            />
                        </Col>
                    </Row>
                </Container>

            </Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    fetchAdminNotifications: () => dispatch(fetchAdminNotifications()),
});

export default connect(null, mapDispatchToProps)(AdminPanel);


