import React, {Component} from 'react';
import {Button, Form, Row, Col} from "reactstrap";
import {connect} from "react-redux";

import FormElement from "../../Components/UI/Form/FormElement";
import {editDoctorCategory, fetchDoctorCategoryAdmin} from "../../store/actions/adminActions";


class DoctorCategoryEditForm extends Component {
    state = {
        doctorCategory: ''
    };

    componentDidMount() {
        this.props.fetchDoctorCategoryAdmin(this.props.match.params.id).then(
            () => {
                this.setState({doctorCategory: this.props.doctorCategoryForEdit.doctorCategory})
            }
        )
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    getFieldHasError = fieldName => {
        return (
            this.props.error &&
            this.props.error.errors &&
            this.props.error.errors[fieldName] &&
            this.props.error.errors[fieldName].message
        );
    };

    submitFormHandler = event => {
        event.preventDefault();
        const data = {...this.state};
        this.props.editDoctorCategory(data, this.props.match.params.id);
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <Row>
                    <Col xs="12" md="6">
                        <FormElement
                            id="editCategory"
                            propertyName="doctorCategory"
                            title="Категория"
                            type="text"
                            value={this.state.doctorCategory}
                            onChange={this.inputChangeHandler}
                            error={this.getFieldHasError('doctorCategory')}
                        >
                        </FormElement>
                    </Col>
                </Row>
                <Button id="sendNewCategory" type="submit" color="success">Сохранить</Button>
            </Form>
        );
    }
}

const mapStateToProps = state => ({
    error: state.admin.error,
    doctorCategoryForEdit: state.admin.doctorCategoryForEdit
});

const mapDispatchToProps = dispatch => ({
    editDoctorCategory: (data, id) => dispatch(editDoctorCategory(data, id)),
    fetchDoctorCategoryAdmin: id => dispatch(fetchDoctorCategoryAdmin(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(DoctorCategoryEditForm);
