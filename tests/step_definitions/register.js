const {I} = inject();
// Add in your custom step files

Given('я нахожусь на странице Регистрация', () => {
  I.amOnPage('register')
});

When('я ввожу {string} в поле {string}', (text, fieldName) => {
  I.fillField({xpath: `//input[@id='${fieldName}']`}, text)
});

When('я выбираю {string} в {string}', (text, fieldName) => {
  I.retry(5).selectOption({xpath:`//select[@id='${fieldName}']`}, text);
});

Then('нажимаю на кнопку {string}', (buttonName) => {
  I.click(`//button[.='${buttonName}']`)
});

When('я вижу текст {string}', (text) => {
  I.waitForText(text);
});


