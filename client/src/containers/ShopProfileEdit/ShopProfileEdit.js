import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import {editProfile} from "../../store/actions/profilesActions";
import ShopProfileForm from "../../Components/ShopProfileForm/ShopProfileForm";



class ShopProfileEdit extends Component {

    editProfile = data => {
        this.props.editProfile(data);
    };

    render() {
        return (
            <Fragment>
                <ShopProfileForm onSubmit={this.props.editProfile}
                                    error={this.props.error}
                                    data={this.props.shopProfile}/>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.profiles.error,
    shopProfile: state.users.shopProfile
});

const mapDispatchToProps = dispatch => ({
    editProfile: (path, profileData) => dispatch(editProfile(path, profileData))
});

export default connect(mapStateToProps, mapDispatchToProps)(ShopProfileEdit);
