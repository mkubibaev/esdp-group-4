const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const DoctorProfileSchema = new Schema({
    name: {
        type: String,
        required: 'Введите имя'
    },
    surname: {
        type: String,
        required: 'Введите фамилию'
    },
    thirdname: {
        type: String,
        required: 'Введите отчество'
    },
    passportId: {
        type: String,
        required: 'Введите айди паспорта'
    },
    passportNumber: {
        type: String,
        required: 'Введите номер паспорта'
    },
    phone: {
        type: String,
        required: 'Введите номер телефона'
    },
    license: String,
    dateOfBirth: {
        type: String,
        required: 'Введите дату рождения'
    },
    yearsOfWork: {
        type: Number,
        min: 0,
        required: 'Введите стаж работы в годах'
    },
    diploma: String,
    clinic: {
        type: Schema.Types.ObjectId,
        ref: 'ClinicProfile',
    },
    schedule: {
        type: String,
        required: 'Введите режим работы'
    },
    gender: {
        type: String,
        required: 'Введите пол',
        enum: ['male', 'female']
    },
    price: Number,
    doctorCategory: {
        type: Schema.Types.ObjectId,
        ref: 'DoctorCategory',
        required: true
    },
    speciality: {
        type: String,
        required: 'Укажите специальность/направления'
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        unique: true
    },
    approved: {
        type: String,
        enum: ['approved', "pending", "declined"],
        default: "pending"
    },
    workAddress: {
        type: String,
        required: 'Введите рабочий адрес'
    },
    rating: [{
        patient: String,
        rating: Number,
        medicalCard: String,
        review: String,
        datetime: String,
        anonymous: Boolean,
        name: String,
        surname: String
    }]
});

const DoctorProfile = mongoose.model('DoctorProfile', DoctorProfileSchema);

module.exports = DoctorProfile;
