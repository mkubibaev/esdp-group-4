const {I} = inject();
// Add in your custom step files


When('я нахожусь на странице Добавить новость', () => {
    I.amOnPage('news/new');
});

When('я ввожу описание новости {string} в поле {string}', (text, fieldName) => {
    I.fillField({xpath:`//textarea[@id='${fieldName}']`}, text)
});

When('я загружаю картинку', () => {
    I.attachFile({xpath:"//input[@id='image']"}, 'dataImage/putin.jpeg');
});

When('я нажимаю кнопку для отправки {string}', (fieldName) => {
    I.click({xpath: `//button[@id='${fieldName}']`});
});




