import React, {Component} from 'react';
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader
} from "reactstrap";
import {loginUser} from "../../store/actions/usersActions";
import {connect} from "react-redux";
import FormElement from "../../Components/UI/Form/FormElement";

const root = {
  display: 'flex',
  flexDirection: 'column',
  alignSelf: 'center',
  width: '80%',
  margin: '20px auto'
};

class Login extends Component {

  state = {
    isOpen: true,
    password: '',
    email: '',
  };

  handleClose = () => {
    this.setState({isOpen: false});
    this.props.history.push('/')
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  submitFormHandler = event => {
    event.preventDefault();
    this.props.loginUser({...this.state});
  };

  render() {
    const {isOpen, password, email} = this.state;
    const {error, className} = this.props
    return (
        <Modal isOpen={isOpen}
               toggle={this.handleClose}
               className={className}
               centered={true}
               size={'lg'}>
          <form style={root} onSubmit={this.submitFormHandler}>
            <ModalHeader toggle={this.toggle}>Вход</ModalHeader>

            <ModalBody>
              <FormElement
                  propertyName="email"
                  type="email"
                  value={email}
                  onChange={this.inputChangeHandler}
                  error={error && error.error}
                  autoComplete="new-email"
                  placeholder="Почтовый адрес"
              />

              <FormElement
                  propertyName="password"
                  type="password"
                  value={password}
                  onChange={this.inputChangeHandler}
                  error={error && error.error}
                  autoComplete="new-password"
                  placeholder="Пароль"
              />
            </ModalBody>
            <ModalFooter>
              <Button type="button" color="danger" className='float-left' onClick={this.handleClose}>
                Отмена
              </Button>
              <Button type="submit" color="primary" className='float-right'>
                Войти
              </Button>
            </ModalFooter>
          </form>

        </Modal>
    );
  }
}

const mapStateToProps = state => ({
  error: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
  loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
