const express = require('express');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const Clinic = require('../models/ClinicProfile');
const Doctor = require('../models/DoctorProfile');
const DoctorCategory = require('../models/DoctorCategory');
const AdminNotification = require('../models/AdminNotification');


const router = express.Router();

router.get('/clinics', [auth, permit('admin')], async (req, res) => {
    try {
        const clinics = await Clinic.find().sort({approved: -1});
        return res.send(clinics);
    } catch(e) {
        return res.status(500).send(e);
    }
});

router.get('/clinic/:id/', [auth, permit('admin')], async (req, res) => {
    try{
        const clinic = await Clinic.findById(req.params.id).sort({approved: -1});
        return res.send(clinic);
    } catch(e){
        return res.status(500).send(e);
    }
});


router.get('/doctors', [auth, permit('admin')], async (req, res) => {
    try {
        const doctors = await Doctor.find();
        return res.send(doctors);
    } catch(e) {
        return res.status(500).send(e)
    }
});

router.get('/doctor/:id/', [auth, permit('admin')], async (req, res) => {
    try{
        const doctor = await Doctor.findById(req.params.id).populate({
            path: "user doctorCategory",
            select: {image: 'image', doctorCategory: 'doctorCategory'}
        });
        return res.send(doctor);
    } catch(e) {
        res.status(500).send(e)
    }
});

router.put('/clinic/:id', [auth, permit('admin')], async (req, res) => {
    try{
        const clinic = await Clinic.findById(req.params.id).populate({
            path: "user",
            select: 'image'
        });
        if (!clinic) {
            return res.sendStatus(404);
        }
        clinic.approved = req.body.approved;

        await clinic.save();
        res.send({message: 'Клиника успешно опубликована'})
    } catch(e){
        res.status(500).send(e)
    }
});

router.put('/doctor/:id', [auth, permit('admin')], async (req, res) => {
    try{
        const doctor = await Doctor.findById(req.params.id);
        if (!doctor) {
            return res.sendStatus(404);
        }
        doctor.approved = req.body.approved;

        await doctor.save();
        res.send({message: 'Профиль доктора одобрен'})
    } catch(e){
        res.status(500).send(e)
    }
});


router.post('/doctor_category', [auth, permit('admin')], async (req, res) => {
    try {
        const doctorCategory = await new DoctorCategory({
            doctorCategory: req.body.doctorCategory
        });
        await doctorCategory.save();
        return res.send({message: 'Категория сохранена'});
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.put('/doctor_category/:id', [auth, permit('admin')], async (req, res) => {
    try {
        const doctorCategory = await DoctorCategory.findById(req.params.id);
        doctorCategory.doctorCategory = req.body.doctorCategory;
        await doctorCategory.save();
        return res.send({message: 'Категория изменена'});
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.delete('/doctor_category/:id', [auth, permit('admin')], async (req, res) => {
    try {
        const doctorCategory = await Doctor.find({doctorCategory: req.params.id});
        if (doctorCategory.length === 0) {
            const deletedCategory = await DoctorCategory.findById(req.params.id);
            deletedCategory.isDeleted = true;
            await deletedCategory.save();
            return res.send({message: 'Категория переведена в список удаленных'});
        } else {
            return res.status(400).send({message: "В данной категории есть врачи, удаление невозможно"})
        }
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.get('/doctor_category', [auth, permit('admin')], async (req, res) => {
    try{
        const doctorCategories = await DoctorCategory.find({isDeleted: false}).sort({doctorCategory: 1});
        return res.send(doctorCategories);
    } catch (e){
        res.status(500).send(e)
    }
});

router.get('/doctor_category/:id', [auth, permit('admin')], async (req, res) => {
    try{
        const doctorCategory= await   DoctorCategory.findById(req.params.id);
        return res.send(doctorCategory);
    } catch(e){
        res.status(500).send(e)
    }
});

router.get('/notifications', auth, async (req, res) => {
    try {
        const notifications = await AdminNotification.find().sort({datetime: -1});
        return res.send(notifications);

    } catch (e) {
        return res.status(500).send(e);
    }
});

router.get('/notifications/:id', auth, (req, res) => {
    AdminNotification.findById(req.params.id)
        .then(notification => {
            if (notification) res.send(notification);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});

router.put('/notifications/:id', auth, async (req, res) => {
    try {
        const notification = await AdminNotification.findById(req.params.id);
        if (!notification) {
            return res.sendStatus(404);
        }
        notification.status = "read";

        await notification.save();
        res.send(notification);
    } catch (e) {
        res.status(500).send(e)
    }
});

router.post('/notifications', async (req, res) => {
    const notification = new AdminNotification({...req.body, datetime: new Date().toISOString()});
    notification.save()
        .then(result => res.send({result, message: "Получено новое уведомление"}))
        .catch(error => res.status(400).send(error));
});

module.exports = router;