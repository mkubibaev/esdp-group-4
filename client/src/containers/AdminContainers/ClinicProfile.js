import React, {Component, Fragment} from 'react';
import Table from "reactstrap/es/Table";
import {connect} from "react-redux";
import {Button} from "reactstrap";

import {approveProfile, declineProfile, fetchClinicIdAdmin} from "../../store/actions/adminActions";
import imageNotAvailable from "../../assets/images/noimage.png";
import config from "../../constants";

class ClinicProfile extends Component {

    componentDidMount() {
        this.props.fetchClinicId(this.props.match.params.id);
    }

    approveProfile = (route, id) => {
        this.props.approveProfile(route, id);
    };

    declineProfile = (route, id) => {
        this.props.declineProfile(route, id);
    };

    render() {
        let image = imageNotAvailable;
        if (this.props.clinic && this.props.clinic.user && this.props.clinic.user.image) {
            image = `${config.apiURL}/uploads/${this.props.clinic.user.image}`
        }
        return (
            <Fragment>
                <div className="user-avatar">
                    <img src={image} alt=""/>
                </div>
                <div className="table-container">
                    <Table bordered>
                        <thead>
                        <tr className="d-flex">
                            <th className="col-3">Аттрибут</th>
                            <th className="col-9">Значение</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr className="d-flex">
                            <td className="col-3">Название Клиники</td>
                            <td className="col-9">{this.props.clinic.title}</td>
                        </tr>
                        <tr className="d-flex">
                            <td className="col-3">Описание Клиники</td>
                            <td className="col-9">{this.props.clinic.description}</td>
                        </tr>
                        <tr className="d-flex">
                            <td className="col-3">Адрес Клиники</td>
                            <td className="col-9">{this.props.clinic.address}</td>
                        </tr>
                        <tr className="d-flex">
                            <td className="col-3">Номер Клиники</td>
                            <td className="col-9">{this.props.clinic.phone}</td>
                        </tr>
                        </tbody>
                    </Table>
                </div>
                <Button id="addClinic" type="button" className="mx-3" color="success"
                        onClick={() => this.approveProfile('clinic', this.props.clinic._id)}>Принять и опубликовать
                    аккаунт</Button>
                <Button id="declineClinic" type="button" color="danger"
                        onClick={() => this.declineProfile('clinic', this.props.clinic._id)}>Отклонить аккаунт</Button>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.admin.error,
    clinic: state.admin.clinicId
});

const mapDispatchToProps = dispatch => ({
    fetchClinicId: id => dispatch(fetchClinicIdAdmin(id)),
    approveProfile: (route, id) => dispatch(approveProfile(route, id)),
    declineProfile: (route, id) => dispatch(declineProfile(route, id))
});

export default connect(mapStateToProps, mapDispatchToProps)(ClinicProfile);
