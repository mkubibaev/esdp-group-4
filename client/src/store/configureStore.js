import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import {createBrowserHistory} from "history";
import axios from '../axios';
import {connectRouter, routerMiddleware} from "connected-react-router";

import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import userReducer from './reducers/usersReducer';
import adminReducer from './reducers/adminReducer';
import profilesReducer from "./reducers/profilesReducer";
import newsReducer from './reducers/newsReducer';
import commentsReducer from './reducers/commentReducer';
import recordsReducer from './reducers/recordsReducer';
import bookingReducer from "./reducers/bookingReducer";
import notificationsReducer from "./reducers/notificationReducer";
import medicalCards from "./reducers/medicalCards";

export const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    users: userReducer,
    profiles: profilesReducer,
    news: newsReducer,
    comments: commentsReducer,
    admin: adminReducer,
    records: recordsReducer,
    notification: notificationsReducer,
    bookingReducer: bookingReducer,
    medicalCards: medicalCards
});


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            user: store.getState().users.user,
            patientProfile: store.getState().users.patientProfile,
            doctorProfile: store.getState().users.doctorProfile,
            clinicProfile: store.getState().users.clinicProfile,
            shopProfile: store.getState().users.shopProfile
        }
    });
});

axios.interceptors.request.use(config => {
    try {
        config.headers['Authorization'] = store.getState().users.user.token;
    } catch (e) {
        //do nothing, user is not logged in
    }

    return config
});

export default store;
