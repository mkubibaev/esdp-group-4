const {I} = inject();
// Add in your custom step files

When('я нахожусь на странице Мои записи', () => {
    I.amOnPage('records/patient');
});

When('я нажимаю на кнопку {string}', (fieldName) => {
    I.click({xpath:`//button[@id='${fieldName}']`});
});

