const config = {
    apiURL: 'http://localhost:8000',
    wsURL: 'ws://localhost:8000'
};


switch (process.env.REACT_APP_ENV) {
    case 'test':
        config.apiURL = 'http://localhost:8010';
        config.wsURL = 'ws://localhost:8010';
        break;
    case 'production':
        config.apiURL = 'https://clinic.serveirc.com:8000';
        config.wsURL = 'ws://clinic.serveirc.com:8000';
        break;
    default:
}

export const path = {
    clinicProfile: "clinicProfiles",
    doctorProfile: "doctorProfiles",
    shopProfile: "shopProfiles",
    patientProfile: "patientProfiles"
};

export default config;
