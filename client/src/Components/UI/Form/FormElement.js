import React from 'react';
import PropTypes from "prop-types";

import {FormFeedback, FormGroup, Input} from "reactstrap";

export const FormElement = ({propertyName, title, error, children, ...props}) => {
    return (
        <FormGroup>
            {title
                ? <label>{title}</label>
                : null
            }
            <Input
                id={propertyName}
                name={propertyName}
                invalid={!!error}
                {...props}
            >
                {children}
            </Input>
            {error && (
                <FormFeedback>
                    {error}
                </FormFeedback>
            )}
        </FormGroup>
    );
};

FormElement.propTypes = {
    propertyName: PropTypes.string.isRequired,
    error: PropTypes.string
};

export default FormElement;
