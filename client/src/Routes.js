import React from 'react';
import {Route, Switch, Redirect} from "react-router-dom";

import RegisterUser from "./containers/Register/RegisterUser";
import Login from "./containers/Login/Login";
import MainPage from "./Components/MainPage/MainPage";
import DoctorPage from "./Components/DoctorPage/DoctorPage";
import UserCabinet from "./containers/UserCabinet/UserCabinet";
import CabinetProfile from "./containers/CabinetProfile/CabinetProfile";
import OneDoctor from "./containers/OneDoctor/OneDoctor";
import ActivateUser from "./containers/ActivateUser/ActivateUser";
import PatientRecords from "./containers/PatientRecords/PatientRecords";
import Booking from "./containers/Booking/Booking";
import DoctorRecords from "./containers/DoctorRecords/DoctorRecords";
import DoctorProfilePage from "./Components/DoctorProfilePage/DoctorProfilePage";
import PatientProfilePage from "./Components/PatientProfilePage/PatientProfilePage";
import ShopProfilePage from "./Components/ShopProfilePage/ShopProfilePage";
import ClinicProfilePage from "./Components/ClinicProfilePage/ClinicProfilePage";
import CreateNews from "./containers/CreateNews/CreateNews";
import SinglePost from "./containers/SinglePost/SinglePost";
import News from "./containers/News/News";
import Notifications from "./containers/Notifications/Notifications";
import OneNotification from "./Components/OneNotification/OneNotification";
import clinicProfile from "./containers/clinicProfile/clinicProfile";
import InfoClinic from "./Components/infoClinic/infoClinic";
import EditPassword from "./Components/EditPassword/EditPassword";
import MedicalCardsList from "./containers/MedicalCardsList/MedicalCardsList";
import MedicalCard from "./Components/MedicalCard/MedicalCard";
import Questionnaire from "./Components/MedicalCard/Questionnaire";
import MedicalCardPage from "./containers/MedicalCardPage/MedicalCardPage";
import PatientMedicalCards from "./containers/PatientMedicalCards/PatientMedicalCards";
import PatientMedicalCardInfo from "./containers/PatientMedicalCardInfo/PatientMedicalCardInfo";

const ProtectedRoute = ({isAllowed, ...props}) => (
    isAllowed ? <Route {...props} /> : <Redirect to="/login"/>
);

const Routes = ({user}) => {
    return (
        <Switch>
            <Route path="/" exact component={MainPage}/>
            <Route path="/news" exact component={News}/>
            <Route path="/news/new" exact component={CreateNews}/>
            <Route path="/news/:id" exact component={SinglePost}/>
            <Route path="/doctors/:id" exact component={OneDoctor}/>
            <Route path="/booking/:id" exact component={Booking}/>
            <Route path="/doctorCategory/:id" exact component={MainPage}/>
            <Route path="/verify/:token" exact component={ActivateUser}/>
            <Route path="/register" exact component={RegisterUser}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/category/doctorPage" component={DoctorPage}/>
            <Route path="/clinicProfile" component={clinicProfile}/>
            <Route path="/infoClinic/:id" exact component={InfoClinic}/>

            <ProtectedRoute
                isAllowed={user && user.role}
                path="/cabinet"
                exact
                component={UserCabinet}
            />
            <ProtectedRoute
                isAllowed={user && user.role}
                path="/cabinet/doctor_profile"
                exact
                component={DoctorProfilePage}
            />
            <ProtectedRoute
                path="/patient_list"
                exact
                component={MainPage}
            />
            <ProtectedRoute
                isAllowed={user && user.profile && user.profile.codeName && user.profile.codeName === 'doctor'}
                path="/patient_list/:id"
                exact
                component={MedicalCardsList}
            />
            <ProtectedRoute
                isAllowed={user && user.profile && user.profile.codeName && user.profile.codeName === 'doctor'}
                path="/patient_list/:id/medical_card"
                exact
                component={MedicalCard}
            />
            <ProtectedRoute
                isAllowed={user && user.profile && user.profile.codeName && user.profile.codeName === 'doctor'}
                path="/patient_list/:id/questionnaire"
                exact
                component={Questionnaire}
            />
            <ProtectedRoute
                isAllowed={user && user.profile && user.profile.codeName && user.profile.codeName === 'doctor'}
                path="/patient_list/:id/questionnaire/edit/:questId"
                exact
                component={Questionnaire}
            />
            <ProtectedRoute
                isAllowed={user && user.profile && user.profile.codeName && user.profile.codeName === 'doctor'}
                path="/patient_list/:id/view/:cardId"
                exact
                component={MedicalCardPage}
            />
            <ProtectedRoute
                isAllowed={user && user.role}
                path="/cabinet/patient_profile"
                exact
                component={PatientProfilePage}
            />
            <ProtectedRoute
                isAllowed={user && user.role}
                path="/cabinet/shop_profile"
                exact
                component={ShopProfilePage}
            />
            <ProtectedRoute
                isAllowed={user && user.role}
                path="/cabinet/clinic_profile"
                exact
                component={ClinicProfilePage}
            />
            <ProtectedRoute
                isAllowed={user && user.role}
                path="/cabinet/profile"
                exact
                component={CabinetProfile}
            />
            <ProtectedRoute
                isAllowed={user && user.role}
                path="/records/patient"
                exact
                component={PatientRecords}
            />
            <ProtectedRoute
                isAllowed={user && user.role}
                path="/records/doctor"
                exact
                component={DoctorRecords}
            />
            <ProtectedRoute
                isAllowed={user && user.role}
                path="/notifications"
                exact
                component={Notifications}
            />
            <ProtectedRoute
                isAllowed={user && user.role}
                path="/notifications/:id"
                exact
                component={OneNotification}
            />
            <ProtectedRoute
                isAllowed={user && user.role}
                path="/cabinet/password"
                exact
                component={EditPassword}
            />
                <ProtectedRoute
                    isAllowed={user && user.profile && user.profile.codeName && user.profile.codeName === 'patient'}
                    path="/cabinet/cards_list"
                    exact
                    component={PatientMedicalCards}
                />
                <ProtectedRoute
                    isAllowed={user && user.profile && user.profile.codeName && user.profile.codeName === 'patient'}
                    path="/cabinet/cards_list/:id"
                    exact
                    component={PatientMedicalCardInfo}
                />
        </Switch>
    )
};

export default Routes;
