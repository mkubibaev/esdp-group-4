const {I} = inject();
// Add in your custom step files

Given('я нохожусь на странице Вход', () => {
  I.amOnPage('login')
});

When('я ввожу {string} в поле {string}', (text, fieldName) => {
  // I.waitForElement({xpath: '' + `//input[@id=${fieldName}]`}, 30);
  I.fillField({xpath: `//input[@id='${fieldName}']`}, text)
});

When('нажимаю на кнопку {string}', (buttonName) => {
  I.click(`//button[.='${buttonName}']`)
});

Then('я вижу текст {string}', (text) => {
  I.waitForText(`//div[contains(@class, 'notification-container')]/span[.='${text}']`);
});





