
import {
    DELETE_RECORDS_AFTER_LOGOUT,
    FETCH_RECORDS_FAILURE,
    FETCH_PATIENT_RECORDS_SUCCESS,
    FETCH_DOCTOR_RECORDS_SUCCESS, OPEN_MODAL
} from "../actions/recordsActions";

const initialState = {
    error: null,
    patientRecords: [], 
    doctorRecords: [],
    modalReschedule: false

};

const recordsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_RECORDS_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case FETCH_PATIENT_RECORDS_SUCCESS:
            return {
                ...state,
                patientRecords: action.data
            };
        case FETCH_DOCTOR_RECORDS_SUCCESS:
            return {
                ...state,
                doctorRecords: action.data
            };
        case DELETE_RECORDS_AFTER_LOGOUT:
            return {
                ...state,
                patientRecords: [],
                doctorRecords: []
            };
        case OPEN_MODAL:
            return {
                ...state,
                modalReschedule: !state.modalReschedule
            };
        default:
            return state;
    }
};
export default recordsReducer;