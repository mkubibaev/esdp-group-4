exports.config = {
    output: './output',
    helpers: {
        Puppeteer: {
            url: 'http://localhost:3010/',
            show: !process.env.CI,
            headless: !!process.env.CI,
        }
    },
    include: {
        I: './steps_file.js'
    },
    mocha: {},
    bootstrap: null,
    teardown: null,
    hooks: [],
    gherkin: {
        features: './features/*.feature',
        steps: [
            './step_definitions/login',
            './step_definitions/register',
            './step_definitions/fillPatientProfile',
            './step_definitions/fillDoctorProfile',
            './step_definitions/recordPatientRevocation',
            './step_definitions/recordPatientConfirm',
            './step_definitions/fillShopProfile',
            './step_definitions/fillClinic',
            './step_definitions/editDoctorProfile',
            './step_definitions/editPatientProfile',
            './step_definitions/editClinicProfile',
            './step_definitions/editShopProfile',
            './step_definitions/recordPatientConfirm',
            './step_definitions/recordPatientRevocation',
            './step_definitions/confirmDoctorAdmin',
            './step_definitions/revocationDoctorAdmin',
            './step_definitions/confirmClinicAdmin',
            './step_definitions/revocationClinicAdmin',
            './step_definitions/deleteDoctorCategory',
            './step_definitions/fillCategoryAdmin',
            './step_definitions/createNews',
            './step_definitions/createComment',
            './step_definitions/editCategory',
        ]
    },
    plugins: {
        screenshotOnFail: {
            enabled: true
        }
    },
    tests: './*_test.js',
    name: 'tests'
};

