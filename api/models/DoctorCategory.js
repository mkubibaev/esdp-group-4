const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const DoctorCategorySchema = new Schema({
    doctorCategory: {
        type: String,
        required: 'Обязательное поле!',
        unique: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
});

const DoctorCategory = mongoose.model('DoctorCategory', DoctorCategorySchema);

module.exports = DoctorCategory;