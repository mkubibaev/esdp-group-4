const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');
const cors = require('cors');
const nanoid = require('nanoid');

const profiles = require('./routes/profiles');
const users = require('./routes/users');
const patientProfiles = require('./routes/patientProfiles');
const clinicProfiles = require('./routes/clinicProfiles');
const shopProfiles = require('./routes/shopProfiles');
const doctorCategories = require('./routes/doctorCategories');
const admin = require('./routes/admin');
const doctorProfiles = require('./routes/doctorProfiles');
const news = require('./routes/news');
const comment = require('./routes/comment');
const record = require('./routes/records');
const notifications = require('./routes/notifications');
const User = require('./models/User');
const PatientProfile = require('./models/PatientProfile');
const DoctorProfile = require('./models/DoctorProfile');
const Record = require('./models/Record');
const medicalCards = require('./routes/medicalCards');
const questionnaire = require('./routes/questionnaire');


const app = express();
const port = process.env.NODE_ENV === 'test' ? 8010 : 8008;

app.use(express.json());

app.use(cors());
app.use(express.static('public'));

const expressWs = require('express-ws')(app);

const activeConnections = {};

app.ws('/notification', async (ws, req) => {

    if (!req.query.token) {
        return ws.close();
    }
    const user = await User.findOne({token: req.query.token});

    if (!user) {
        return ws.close();
    }

    const id = nanoid();
    activeConnections[id] = {ws, user};

    ws.on('message', async msg => {
        const decodedMessage = JSON.parse(msg);
        switch (decodedMessage.type) {
            case 'CREATE_MESSAGE':

                let targetUserMessage;
                if (decodedMessage.profile === "patient") {
                    targetUserMessage = await DoctorProfile.findById(decodedMessage.doctorId).populate('user');
                    if (!targetUserMessage) {
                        break;
                    }
                }
                if (decodedMessage.profile === "doctor") {
                    targetUserMessage = await PatientProfile.findById(decodedMessage.patientId).populate('user');
                    if (!targetUserMessage) {
                        break;
                    }
                }

                const message = await JSON.stringify({
                    type: 'NEW_MESSAGE',
                    data: "Создана новая запись в календаре"
                });

                Object.keys(activeConnections).forEach(connId => {
                    if (activeConnections[connId].user._id.equals(targetUserMessage.user._id)) {
                        let conn = activeConnections[connId].ws;
                        conn.send(message);
                    }
                });

                break;
            case 'CHANGE_RECORD':
                let model;
                let targetUser;
                model = await Record.findById(decodedMessage.id).populate('patientId doctorId');
                if (decodedMessage.profile === "patient") {
                    targetUser = model.doctorId.user;
                }
                if (decodedMessage.profile === "doctor") {
                    targetUser = model.patientId.user;
                }

                const notification = await JSON.stringify({
                    type: 'NEW_MESSAGE',
                    data: "Статус вашей записи изменен"
                });

                Object.keys(activeConnections).forEach(connId => {
                    if (activeConnections[connId].user._id.equals(targetUser)) {
                        let conn = activeConnections[connId].ws;
                        conn.send(notification);
                    }
                });
                break;
            case 'RESCHEDULE_RECORD':
                let record;
                let patientID;
                let doctorID;
                record = await Record.findById(decodedMessage.id).populate('patientId doctorId');
                if (decodedMessage.profile === "patient") {
                    doctorID = record.doctorId.user;
                }
                if (decodedMessage.profile === "doctor") {
                    patientID = record.patientId.user;
                }

                const notify = await JSON.stringify({
                    type: 'NEW_MESSAGE',
                    data: "Дата записи изменена"
                });

                Object.keys(activeConnections).forEach(connId => {
                    if (activeConnections[connId].user._id.equals(doctorID)) {
                        let conn = activeConnections[connId].ws;
                        conn.send(notify);
                    }
                    if (activeConnections[connId].user._id.equals(patientID)) {
                        let conn = activeConnections[connId].ws;
                        conn.send(notify);
                    }

                });
                break;
            case 'ADMIN_NOTIFICATION':

                let admin = await User.findOne({role: "admin"});
                let profile;
                if (decodedMessage.profile === "doctor") {
                    profile = "doctor"
                } else {
                    profile = "clinic"
                }
                const adminNotification = await JSON.stringify({
                    type: 'NEW_MESSAGE',
                    data: "Создан новый аккаунт",
                    profile: profile
                });

                Object.keys(activeConnections).forEach(connId => {
                    if (activeConnections[connId].user._id.equals(admin._id)) {
                        let conn = activeConnections[connId].ws;
                        conn.send(adminNotification);
                    }
                });
                break;
            default:
                break;
        }
    });

    ws.on('close', () => {
        delete activeConnections[id];
    })
});

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/profiles', profiles);
    app.use('/users', users);
    app.use('/patientProfiles', patientProfiles);
    app.use('/clinicProfiles', clinicProfiles);
    app.use('/shopProfiles', shopProfiles);
    app.use('/doctorCategories', doctorCategories);
    app.use('/admin', admin);
    app.use('/doctorProfiles', doctorProfiles);
    app.use('/news', news);
    app.use('/comment', comment);
    app.use('/records', record);
    app.use('/medical_cards', medicalCards);
    app.use('/questionnaire', questionnaire);
    app.use('/notifications', notifications);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });

});
