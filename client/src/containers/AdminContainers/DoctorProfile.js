import React, {Component, Fragment} from 'react';
import Table from "reactstrap/es/Table";
import {connect} from "react-redux";
import {approveProfile, declineProfile, fetchDoctorIdAdmin} from "../../store/actions/adminActions";
import {Button} from "reactstrap";
import imageNotAvailable from "../../assets/images/noimage.png";
import config from "../../constants";

class DoctorProfile extends Component {

    componentDidMount() {
        this.props.fetchDoctorId(this.props.match.params.id);
    }

    approveProfile = (route, id) => {
        this.props.approveProfile(route, id);
    };

    declineProfile = (route, id) => {
        this.props.declineProfile(route, id);
    };

    render() {
        let image = imageNotAvailable;
        if (this.props.doctor && this.props.doctor.user && this.props.doctor.user.image) {
            image = `${config.apiURL}/uploads/${this.props.doctor.user.image}`
        }
        return (
            <Fragment>
                <div className="user-avatar">
                    <img src={image} alt=""/>
                </div>
                <div className="table-container">
                <Table  bordered>
                    <thead>
                    <tr className="d-flex">
                        <th className="col-3">Аттрибут</th>
                        <th className="col-9">Значение</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr className="d-flex">
                        <td className="col-3">Имя</td>
                        <td className="col-9">{this.props.doctor.name}</td>
                    </tr>
                    <tr className="d-flex">
                        <td className="col-3">Фамилия</td>
                        <td className="col-9">{this.props.doctor.surname}</td>
                    </tr>
                    <tr className="d-flex">
                        <td className="col-3">Отчество</td>
                        <td className="col-9">{this.props.doctor.thirdname}</td>
                    </tr>
                    <tr className="d-flex">
                        <td className="col-3">Серия и номер паспорта</td>
                        <td className="col-9">{this.props.doctor.passportId} {this.props.doctor.passportNumber}</td>
                    </tr>
                    <tr className="d-flex">
                        <td className="col-3">Дата рождения</td>
                        <td className="col-9">{this.props.doctor.dateOfBirth}</td>
                    </tr>
                    <tr className="d-flex">
                        <td className="col-3">Лицензия</td>
                        <td className="col-9">{this.props.doctor.license}</td>
                    </tr>
                    <tr className="d-flex">
                        <td className="col-3">Стаж работы</td>
                        <td className="col-9">{this.props.doctor.yearsOfWork}</td>
                    </tr>
                    <tr className="d-flex">
                        <td className="col-3">Специальность</td>
                        <td className="col-9">{this.props.doctor.speciality}</td>
                    </tr>
                    <tr className="d-flex">
                        <td className="col-3">Направление</td>
                        <td className="col-9">{this.props.doctor.doctorCategory ? this.props.doctor.doctorCategory.doctorCategory : null}</td>
                    </tr>
                    <tr className="d-flex">
                        <td className="col-3">Диплом</td>
                        <td className="col-9">{this.props.doctor.diploma}</td>
                    </tr>
                    <tr className="d-flex">
                        <td className="col-3">Режим работы</td>
                        <td className="col-9">{this.props.doctor.schedule}</td>
                    </tr>
                    <tr className="d-flex">
                        <td className="col-3">Пол</td>
                        <td className="col-9">{this.props.doctor.gender === "male" ? "мужской" : "женский"}</td>
                    </tr>
                    </tbody>
                </Table>
                </div>
                <Button id="addDoctor" className="mx-3" color="success"  onClick={() => this.approveProfile('doctor', this.props.doctor._id)}>Принять и опубликовать аккаунт</Button>
                <Button id="declineDoctor" color="danger" onClick={() => this.declineProfile('doctor', this.props.doctor._id)}>Отклонить аккаунт</Button>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.admin.error,
    doctor: state.admin.doctorId
});

const mapDispatchToProps = dispatch => ({
    fetchDoctorId: id => dispatch(fetchDoctorIdAdmin(id)),
    approveProfile: (route, id) => dispatch(approveProfile(route, id)),
    declineProfile: (route, id) => dispatch(declineProfile(route, id))
});

export default connect(mapStateToProps, mapDispatchToProps)(DoctorProfile);
