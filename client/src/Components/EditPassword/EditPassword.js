import React, {Component} from 'react';
import FormElement from "../UI/Form/FormElement";
import {Button, Col, Form, Row} from "reactstrap";
import {connect} from "react-redux";
import {editPassword} from "../../store/actions/usersActions";


class EditPassword extends Component {
    state = {
        password: "",
        oldPassword: ""
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    getFieldHasError = fieldName => {
        return (
            this.props.error &&
            this.props.error.errors &&
            this.props.error.errors[fieldName] &&
            this.props.error.errors[fieldName].message
        );
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.props.editPassword({...this.state});
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <Row>
                    <Col xs='12'>
                        <FormElement
                            propertyName="oldPassword"
                            title="Введите старый пароль"
                            type="password"
                            value={this.state.oldPassword}
                            onChange={this.inputChangeHandler}
                            error={this.getFieldHasError('oldPassword')}
                            autoComplete="new-password"
                            placeholder="Старый пароль"
                        />
                    </Col>
                    <Col xs='12'>
                        <FormElement
                            propertyName="password"
                            title="Введите новый пароль"
                            type="password"
                            value={this.state.password}
                            onChange={this.inputChangeHandler}
                            error={this.getFieldHasError('password')}
                            autoComplete="new-password"
                            placeholder="Новый пароль"
                        />
                    </Col>
                    <Button type="submit" color="primary">Сохранить</Button>
                </Row>
            </Form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    editPassword: data => dispatch(editPassword(data))
});

export default connect(null, mapDispatchToProps)(EditPassword);