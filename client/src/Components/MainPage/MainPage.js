import React, {Fragment, Component} from 'react';
import {Col, Form, ListGroupItem, Nav, NavItem, Row,} from "reactstrap";
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import "./MainPage.css";
import {fetchDoctorCategories, fetchDoctors, searchData, sortData} from "../../store/actions/profilesActions";
import Thumbnail from "../Thumbnail/Thumbnail";
import FormElement from "../UI/Form/FormElement";


class MainPage extends Component {

    state = {
        search: "",
        filter: ""
    };

    inputChangeHandler = event => {
        let filter = event.target.value.toLowerCase().trim();
        this.setState({
            [event.target.name]: event.target.value,
        });
        this.props.searchData(filter, this.props.match.params.id);
    };

    selectChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value,
        });
        this.props.sortData(event.target.value);
    };

    componentDidMount() {
        this.props.fetchDoctors(this.props.match.params.id);
        this.props.fetchCategories();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.id !== this.props.match.params.id) {
            this.props.fetchDoctors(this.props.match.params.id);
        }
    }

    averageRating = data => {
        const ratingsNum = data.length;
        const sum = data.reduce((acc, currentVal) => acc += currentVal.rating || 0, 0);
        return (sum === 0 && ratingsNum === 0) ? "Нет оценок" : "Средний рейтинг: " + Math.round(sum / ratingsNum);

    };

    render() {
        return (
            <Fragment>
                <div className="mainPage">
                    <div className='main'>
                        <Nav>
                            {this.props.doctorCategories.map(category => (
                                <NavItem className='li' key={category._id}>
                                    <ListGroupItem tag={NavLink}
                                                   to={'/doctorCategory/' + category._id}>
                                        {category.doctorCategory}
                                    </ListGroupItem>
                                </NavItem>
                            ))}
                        </Nav>
                    </div>
                    <h3 className="text-center">Врачи</h3>
                    <Form>
                        <Row>
                            <Col xs='12' md="6">
                                <FormElement
                                    propertyName="search"
                                    placeholder="Поиск доктора по имени либо фамилии"
                                    type="text"
                                    value={this.state.search}
                                    onChange={this.inputChangeHandler}
                                />
                            </Col>
                            <Col xs='12' md="6">
                                <FormElement
                                    propertyName="filter"
                                    type="select"
                                    value={this.state.filter}
                                    onChange={this.selectChangeHandler}
                                >
                                    <option value="">Сортировать по</option>
                                    <option value="rating">Рейтинг по убыванию</option>
                                    <option value="high-price">Цена по возрастанию</option>
                                    <option value="low-price">Цена по убыванию</option>
                                    <option value="gender">Пол</option>
                                </FormElement>
                            </Col>
                        </Row>
                    </Form>
                    <div className="row mt-30">
                        {this.props.doctors.map((doctor, ndx) => (
                            <div key={doctor._id} className="col-md-4 col-sm-6">
                                <div className="box16">
                                    <Thumbnail image={doctor.user.image}/>
                                    <div className="box-content">
                                        <h3 className="title">{doctor.name} {doctor.surname}</h3>
                                        <p className="post">{doctor.speciality}</p>
                                        <p className="rating"> {this.averageRating(doctor.rating)}</p>
                                        <NavItem className="profile">
                                            <ListGroupItem tag={NavLink}
                                                           to={'/doctors/' + doctor._id}>
                                                <span id={ndx}>Посмотреть профиль</span>
                                            </ListGroupItem>
                                        </NavItem>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </Fragment>
        );
    }
}


const mapStateToProps = state => ({
    doctors: state.profiles.doctors,
    doctorCategories: state.profiles.doctorCategories,
});

const mapDispatchToProps = dispatch => ({
    fetchDoctors: categoryId => dispatch(fetchDoctors(categoryId)),
    fetchCategories: () => dispatch(fetchDoctorCategories()),
    searchData: filter => dispatch(searchData(filter)),
    sortData: sort => dispatch(sortData(sort))
});

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
