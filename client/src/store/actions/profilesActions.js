import axios from '../../axios';
import {push} from 'connected-react-router';
import {NotificationManager} from 'react-notifications';
import {
    FETCH_PROFILES_SUCCESS,
    FETCH_DATA_FAILURE,
    FETCH_DATA_REQUEST,
    ADD_DATA_REQUEST,
    ADD_DATA_FAILURE,
    ADD_DATA_SUCCESS,
    FETCH_CATEGORY_SUCCESS,
    FETCH_CLINICS_LIST_SUCCESS,
    FETCH_DOCTORS_SUCCESS,
    FETCH_DOCTOR_SUCCESS,
    FETCH_PATIENT_SUCCESS,
    FETCH_PROFILE_FAILURE,
    DELETE_DATA_AFTER_LOGOUT,
    FETCH_DOCTOR_PROFILE_SUCCESS,
    FETCH_SHOP_PROFILE_SUCCESS,
    FETCH_CLINIC_PROFILE_SUCCESS,
    SEARCH_DATA,
    FETCH_CLINIC_DOCTOR_PROFILE_SUCCESS,
    FETCH_CLINIC_INFO_SUCCESS,
    FETCH_PATIENT_FOR_RECORD,
    FETCH_CLINIC_DOCTORS_SUCCESS
} from "./actionTypes";

import {path} from "../../constants";

const fetchDataRequest = () => ({type: FETCH_DATA_REQUEST});
const fetchDataFailure = error => ({type: FETCH_DATA_FAILURE, error});

const addDataRequest = () => ({type: ADD_DATA_REQUEST});
const addDataFailure = error => ({type: ADD_DATA_FAILURE, error});
const addDataSuccess = () => ({type: ADD_DATA_SUCCESS});

const fetchProfilesSuccess = profiles => ({type: FETCH_PROFILES_SUCCESS, profiles});

const fetchDoctorsSuccess = doctors => ({type: FETCH_DOCTORS_SUCCESS, doctors});
const fetchDoctorSuccess = doctor => ({type: FETCH_DOCTOR_SUCCESS, doctor});

const fetchDoctorProfileSuccess = data => ({type: FETCH_DOCTOR_PROFILE_SUCCESS, data});

const fetchShopProfileSuccess = data => ({type: FETCH_SHOP_PROFILE_SUCCESS, data});

const fetchClinicProfileSuccess = data => ({type: FETCH_CLINIC_PROFILE_SUCCESS, data});
const fetchClinicDoctorsProfileSuccess = data => ({type: FETCH_CLINIC_DOCTORS_SUCCESS, data});

const fetchCategorySuccess = data => ({type: FETCH_CATEGORY_SUCCESS, data});
const fetchClinicsSuccess = data => ({type: FETCH_CLINICS_LIST_SUCCESS, data});

const fetchClinicDoctorProfile = data => ({type: FETCH_CLINIC_DOCTOR_PROFILE_SUCCESS, data});
const infoClinicSuccess = data => ({type: FETCH_CLINIC_INFO_SUCCESS, data});

const fetchPatientSuccess = data => {
    return {type: FETCH_PATIENT_SUCCESS, data};
};

const fetchPatientForRecord = data => {
    return {type: FETCH_PATIENT_FOR_RECORD, data}
};

const fetchProfileFailure = error => ({type: FETCH_PROFILE_FAILURE, error});

export const deleteDataAfterLogout = () => {
    return {type: DELETE_DATA_AFTER_LOGOUT};
};

const saveSearchData = filterData => {
    return {type: SEARCH_DATA, filterData}
};

export const searchData = (filter, categoryId) => {
    return async dispatch => {
        try {
            let paths = `/${path.doctorProfile}`;

            if (categoryId) {
                paths += '?doctorCategory=' + categoryId;
            }
            const response = await axios.get(paths);
            const filterData = response.data.filter(doctor => {
                return doctor.name.toLowerCase().includes(filter) || doctor.surname.toLowerCase().includes(filter)
            });
            dispatch(saveSearchData(filterData));
        } catch (e) {
            dispatch(fetchDataFailure(e));
        }
    };
};

export const sortData = (filter) => {
    return (dispatch, getState) => {
        try {
            let state = getState();
            let filterData;
            if (filter === "high-price") {
                filterData = state.profiles.doctors.sort((a, b) => {
                    return a.price - b.price
                });
            } else if (filter === "rating") {
                let rating = state.profiles.doctors.map(doctor => {
                    const ratingsNum = doctor.rating.length;
                    const sum = doctor.rating.reduce((acc, currentVal) => acc += currentVal.rating || 0, 0);
                    doctor.rating.rating = (sum === 0 && ratingsNum === 0) ? 0 : sum / ratingsNum;
                    return doctor;
                });
                filterData = rating.sort((a, b) => {
                    return b.rating.rating - a.rating.rating
                });
            } else if (filter === "low-price") {
                filterData = state.profiles.doctors.sort((a, b) => {
                    return b.price - a.price
                });
            } else if (filter === "gender") {
                filterData = state.profiles.doctors.sort((a, b) => {
                    if (a.gender > b.gender) {
                        return 1
                    }
                    return -1
                });
            } else {
                filterData = state.profiles.doctors;
            }
            dispatch(saveSearchData(filterData));
        } catch (e) {
            dispatch(fetchDataFailure(e));
        }
    };
};


export const fetchProfiles = () => {
    return async dispatch => {
        dispatch(fetchDataRequest());

        try {
            const response = await axios.get('/profiles');
            dispatch(fetchProfilesSuccess(response.data));
        } catch (e) {
            dispatch(fetchDataFailure(e));
        }
    }
};

export const fetchProfileDoctorClinic = () => {
    return async dispatch => {
        dispatch(fetchDataRequest());

        try {
            const response = await axios.get('/clinicProfiles');
            dispatch(fetchClinicDoctorProfile(response.data));
        } catch (e) {
            dispatch(fetchDataFailure(e));
        }
    }
};

export const infoClinic = id => {
    return async dispatch => {
        dispatch(fetchDataRequest());

        try {
            const response = await axios.get(`/clinicProfiles/${id}`);
            dispatch(infoClinicSuccess(response.data));
        } catch (error) {
            dispatch(fetchDataFailure(error));
        }
    };
};

export const fetchDoctors = (categoryId) => {
    return async dispatch => {
        dispatch(fetchDataRequest());

        try {
            let paths = `/${path.doctorProfile}`;

            if (categoryId) {
                paths += '?doctorCategory=' + categoryId;
            }
            const response = await axios.get(paths);
            dispatch(fetchDoctorsSuccess(response.data));
        } catch (e) {
            dispatch(fetchDataFailure(e));
        }
    }
};

export const fetchClinicsDoctors = (clinic) => {
    return async dispatch => {
        dispatch(fetchDataRequest());

        try {
            let paths = `/${path.doctorProfile}`;

            if (clinic) {
                paths += '?clinic=' + clinic;
            }
            const response = await axios.get(paths);
            dispatch(fetchClinicDoctorsProfileSuccess(response.data));
        } catch (e) {
            dispatch(fetchDataFailure(e));
        }
    }
};

export const fetchDoctorPublic = id => {
    return dispatch => {
        return axios.get(`/${path.doctorProfile}/${id}`).then(response => {
            dispatch(fetchDoctorSuccess(response.data));
        })
    }
};

export const fetchDoctorCategories = () => {
    return dispatch => {
        return axios.get('/doctorCategories').then(
            response => dispatch(fetchCategorySuccess(response.data)),
            error => dispatch(fetchDataFailure(error))
        );
    };
};

export const fetchClinics = () => {
    return dispatch => {
        return axios.get(`/${path.clinicProfile}`).then(
            response => dispatch(fetchClinicsSuccess(response.data)),
            error => dispatch(fetchDataFailure(error))
        );
    };
};

export const addProfile = (paths, profileData) => {
    return async (dispatch, getState) => {
        dispatch(addDataRequest());
        try {
            const state = getState();
            const response = await axios.post(`/${paths}`, profileData);

            dispatch(addDataSuccess());
            switch (paths) {
                case path.doctorProfile:
                    dispatch(fetchDoctorProfile());
                    dispatch(push('/cabinet/doctor_profile'));
                    state.users.websocket.send(JSON.stringify({type: "ADMIN_NOTIFICATION", profile: "doctor"}));
                    break;
                case path.patientProfile:
                    dispatch(fetchPatientProfile());
                    dispatch(push('/cabinet/patient_profile'));
                    break;
                case path.shopProfile:
                    dispatch(fetchShopProfile());
                    dispatch(push('/cabinet/shop_profile'));
                    break;
                case path.clinicProfile:
                    dispatch(fetchClinicProfile());
                    dispatch(push('/cabinet/clinic_profile'));
                    state.users.websocket.send(JSON.stringify({type: "ADMIN_NOTIFICATION", profile: "clinic"}));
                    break;
                default:
                    break;
            }
            NotificationManager.success(response.data.message);
        } catch (e) {
            dispatch(addDataFailure(e));
        }
    }
};


export const fetchPatientProfile = (name, surname) => {
    return async dispatch => {
        try {
            if (name) {
                const data = await axios.get(`/${path.patientProfile}?name=${name}`);
                if (data.data === "") {
                    NotificationManager.warning("Не удалось ничего найти. Попробуйте снова");
                }
                dispatch(fetchPatientForRecord(data.data));
            }
            else if (surname) {
                const result = await axios.get(`/${path.patientProfile}?surname=${surname}`);
                if (result.data === "") {
                    NotificationManager.warning("Не удалось ничего найти. Попробуйте снова");
                }
                dispatch(fetchPatientForRecord(result.data));
            }
           else if (surname && name) {
                const patient = await axios.get(`/${path.patientProfile}?surname=${surname}&name=${name}`);
                if (patient.data === "") {
                    NotificationManager.warning("Не удалось ничего найти. Попробуйте снова");
                }
                dispatch(fetchPatientForRecord(patient.data));
            } else {
                const response = await axios.get(`/${path.patientProfile}`);
                dispatch(fetchPatientSuccess(response.data));
            }
        } catch (e) {
            dispatch(fetchProfileFailure(e));
        }
    }
};

export const fetchShopProfile = () => {
    return async dispatch => {
        try {
            const response = await axios.get(`/${path.shopProfile}`);
            dispatch(fetchShopProfileSuccess(response.data));
        } catch (e) {
            dispatch(fetchProfileFailure(e));
        }
    }
};

export const fetchClinicProfile = () => {
    return async (dispatch, getState) => {
        try {
            const state = getState();
            const user = state.users.user._id;
            const response = await axios.get(`/${path.clinicProfile}?user=${user}`);
            dispatch(fetchClinicProfileSuccess(response.data[0]));
        } catch (e) {
            dispatch(fetchProfileFailure(e));
        }
    }
};

export const fetchDoctorProfile = () => {
    return async (dispatch, getState) => {
        try {
            const state = getState();
            const user = state.users.user._id;
            const response = await axios.get(`/${path.doctorProfile}?user=${user}`);
            dispatch(fetchDoctorProfileSuccess(response.data[0]));
        } catch (e) {
            dispatch(fetchProfileFailure(e));
        }
    }
};

export const editProfile = (paths, profileData) => {
    return async dispatch => {
        dispatch(addDataRequest());

        try {
            const response = await axios.put(`/${paths}`, profileData);

            dispatch(addDataSuccess());
            switch (paths) {
                case path.doctorProfile:
                    dispatch(fetchDoctorProfile());
                    dispatch(push('/cabinet/doctor_profile'));
                    break;
                case path.patientProfile:
                    dispatch(fetchPatientProfile());
                    dispatch(push('/cabinet/patient_profile'));
                    break;
                case path.shopProfile:
                    dispatch(fetchShopProfile());
                    dispatch(push('/cabinet/shop_profile'));
                    break;
                case path.clinicProfile:
                    dispatch(fetchClinicProfile());
                    dispatch(push('/cabinet/clinic_profile'));
                    break;
                default:
                    break;
            }
            NotificationManager.success(response.data.message);
        } catch (e) {
            dispatch(addDataFailure(e));
        }
    }
};

export const sendRating = (id, data) => {
    return dispatch => {
        return axios.put(`/${path.doctorProfile}/${id}`, data).then(
            response => {
                dispatch(addDataSuccess());
                NotificationManager.success(response.data.message);
                dispatch(fetchDoctorPublic(id));
                dispatch(push('/doctors/' + id));
            },
            error => {
                NotificationManager.warning(error.response.data);
            }
        );
    };
};