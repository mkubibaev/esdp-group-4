import React, {Component, Fragment} from 'react';
import {Button, Table} from "reactstrap";
import {connect} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleDoubleRight} from "@fortawesome/free-solid-svg-icons";

import {fetchDoctorsAdmin} from "../../store/actions/adminActions";


class AdminDoctorsList extends Component {

    componentDidMount() {
        this.props.fetchDoctorsAdmin();
    }


    goToProfile = (route, id) => {
        this.props.history.push({
            pathname: '/admin/' + route + '/' + id
        })
    };

    render() {
        return (
            <Fragment>
                <h2 className="my-3">Список врачей</h2>
                <div className="table-container">
                    <Table bordered>
                        <thead>
                        <tr>
                            <th>Номер</th>
                            <th>Имя</th>
                            <th>Статус подтверждения</th>
                            <th>Действие</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.props.doctors.map((doctor, index) => {
                            let status;
                            switch (doctor.approved) {
                                case 'approved':
                                    status = "подтвержден";
                                    break;
                                case 'declined':
                                    status = "отклонен";
                                    break;
                                case 'pending':
                                    status = "в ожидании";
                                    break;
                                default:
                                    break;
                            }
                            return <tr key={doctor._id}>
                                <td>{index + 1}</td>
                                <td>{doctor.name} {doctor.surname} {doctor.thirdname}</td>
                                <td>{status}</td>
                                <td>
                                    <Button id="detailed" type="button" color="warning"
                                            onClick={() => this.goToProfile('doctor', doctor._id)}>Подробнее <FontAwesomeIcon
                                        icon={faAngleDoubleRight}/></Button>
                                </td>
                            </tr>
                        })}
                        </tbody>
                    </Table>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.admin.error,
    doctors: state.admin.doctors
});

const mapDispatchToProps = dispatch => ({
    fetchDoctorsAdmin: () => dispatch(fetchDoctorsAdmin())
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminDoctorsList);