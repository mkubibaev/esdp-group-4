import React, {Component, Fragment} from 'react';
import {Button, Form, Row, Col, FormGroup} from "reactstrap";
import FormElement from "../UI/Form/FormElement";
import DatePicker from "react-datepicker";
import inputMask from "react-input-mask";

import {path} from "../../constants";
import {Link} from "react-router-dom";
import UserCabinetMenu from "../UserCabinetMenu/UserCabinetMenu";

class DoctorProfileForm extends Component {

    constructor(props) {
        super(props);
        if (props.data) {
            this.state = {...props.data};
        } else {
            this.state = {
                name: '',
                surname: '',
                thirdname: '',
                passportId: '',
                passportNumber: '',
                diploma: '',
                speciality: '',
                license: '',
                clinic: '',
                schedule: '',
                phone: '',
                dateOfBirth: '',
                yearsOfWork: '',
                gender: '',
                price: '',
                doctorCategory: '',
                workAddress: ''
            };
        }
    }


    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    dateChangeHandler = date => {
        this.setState({
            dateOfBirth: date
        });
    };

    dateFormat = date => {
        let d = new Date(date);

        return d.toLocaleDateString('ru-GB');
    };

    getFieldHasError = fieldName => {
        return (
            this.props.error &&
            this.props.error.errors &&
            this.props.error.errors[fieldName] &&
            this.props.error.errors[fieldName].message
        );
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.onSubmit(path.doctorProfile, {...this.state});
    };

    render() {
        return (
            <Row>
                <Col xs="12" md="3">
                    <UserCabinetMenu/>
                </Col>
                <Col xs="12" md="9">
                    <Form onSubmit={this.submitFormHandler}>
                        <Row>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="name"
                                    title="Введите ваше имя:"
                                    type="text"
                                    value={this.state.name}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('name')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="surname"
                                    title="Введите вашу фамилию:"
                                    type="text"
                                    value={this.state.surname}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('surname')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="thirdname"
                                    title="Введите ваше отчество:"
                                    type="text"
                                    value={this.state.thirdname}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('thirdname')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="passportId"
                                    title="Введите id паспорта:"
                                    type="text"
                                    value={this.state.passportId}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('passportId')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="passportNumber"
                                    title="Введите номер паспорта:"
                                    type="text"
                                    value={this.state.passportNumber}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('passportNumber')}
                                />
                            </Col>
                        </Row>
                        <FormElement
                            propertyName="doctorCategory"
                            title="Категория"
                            type="select"
                            value={this.state.doctorCategory}
                            onChange={this.inputChangeHandler}
                            error={this.getFieldHasError('doctorCategory')}
                        >
                            <option value=""/>
                            {this.props.doctorCategories.map(category => (
                                    <option key={category._id}
                                            value={category._id}
                                    >
                                        {category.doctorCategory}
                                    </option>
                                )
                            )}
                        </FormElement>
                        <FormElement
                            propertyName="diploma"
                            title="Диплом:"
                            type="text"
                            value={this.state.diploma}
                            onChange={this.inputChangeHandler}
                            error={this.getFieldHasError('diploma')}
                        />
                        <FormElement
                            propertyName="speciality"
                            title="Специализация:"
                            type="textarea"
                            value={this.state.speciality}
                            onChange={this.inputChangeHandler}
                            error={this.getFieldHasError('speciality')}
                        />
                        <Row>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="yearsOfWork"
                                    title="Стаж работы (годы):"
                                    type="number"
                                    value={this.state.yearsOfWork}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('yearsOfWork')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    type="select"
                                    title="Выберите пол:"
                                    propertyName="gender"
                                    value={this.state.gender}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('gender')}
                                >
                                    <option value=""/>
                                    <option value="male">Мужской</option>
                                    <option value="female">Женский</option>
                                </FormElement>
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="schedule"
                                    title="Режим работы:"
                                    type="text"
                                    value={this.state.schedule}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('schedule')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="phone"
                                    title="Номер телефона:"
                                    mask="+\9\96 (999) 99-99-99"
                                    tag={inputMask}
                                    maskChar="_"
                                    type="phone"
                                    value={this.state.phone}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('phone')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="license"
                                    title="Номер лицензии:"
                                    type="text"
                                    value={this.state.license}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('license')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    type="select"
                                    title="Клиника:"
                                    propertyName="clinic"
                                    value={this.state.clinic}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('clinic')}
                                >
                                    <option value=""/>
                                    {this.props.clinics.map(clinic => (
                                            <option key={clinic._id}
                                                    value={clinic._id}
                                            >
                                                {clinic.title}
                                            </option>
                                        )
                                    )}
                                </FormElement>
                            </Col>
                            <Col xs="12" md="6">
                                <FormGroup>
                                    {this.props.data ?
                                        <FormElement
                                            propertyName="dateOfBirth"
                                            title="Дата рождения"
                                            type="text"
                                            value={this.dateFormat(this.state.dateOfBirth)}
                                            readOnly
                                            error={this.getFieldHasError('dateOfBirth')}
                                        />

                                        : <Fragment>
                                            <label>Дата рождения:</label>
                                            <DatePicker
                                                onChange={this.dateChangeHandler}
                                                selected={this.state.dateOfBirth}
                                                className="form-control"
                                            />
                                        </Fragment>}
                                </FormGroup>
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="price"
                                    title="Стоимость услуг (от):"
                                    tag={inputMask}
                                    maskChar="_"
                                    type="number"
                                    value={this.state.price}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('price')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="workAddress"
                                    title="Введите рабочий адрес"
                                    type="text"
                                    value={this.state.workAddress}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('workAddress')}
                                />
                            </Col>
                        </Row>
                        <Link to='/cabinet'>
                            <Button className="mr-3" color="secondary">
                                Отмена
                            </Button>
                        </Link>
                        <Button id="saveDoctorProfile" type="submit" color="primary">Сохранить</Button>
                    </Form>
                </Col>
            </Row>

        );
    }
}


export default DoctorProfileForm;
