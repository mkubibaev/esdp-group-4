import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faClock} from "@fortawesome/free-solid-svg-icons";
import {faCalendarWeek} from "@fortawesome/free-solid-svg-icons";

import {Table} from "reactstrap";
import {changeAdminNotificationStatus, fetchAdminNotifications} from "../../store/actions/adminActions";

class AdminNotificationsList extends Component {

    componentDidMount() {
        this.props.fetchAdminNotifications();
    }

    dateFormat = date => {
        let d = new Date(date);
        return <p><FontAwesomeIcon
            icon={faCalendarWeek}/> {d.toLocaleDateString('ru-GB')} <br/>
            <FontAwesomeIcon icon={faClock}/> {d.toLocaleTimeString()}</p>;
    };

    changeStatus = id => {
        this.props.changeAdminNotificationStatus(id);
    };

    render() {
        return (
            <Fragment>
                {this.props.adminNotifications.length > 0 ?
                    <div className="table-container">
                        <Table bordered>
                            <thead>
                            <tr>
                                <th scope="row">№</th>
                                <th scope="row">Дата</th>
                                <th scope="row">Текст</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.props.adminNotifications.map((message, index) => {
                                return <tr key={message._id}>
                                    <td>{index + 1}</td>
                                    <td>{this.dateFormat(message.datetime)} </td>
                                    <td><p onClick={() => this.changeStatus(message._id)}>
                                        <span
                                            className={message.status === "new" ? "New" : "Read"}>{message.text}</span>
                                    </p></td>
                                </tr>
                            })}
                            </tbody>
                        </Table>
                    </div>
                    : <h3>У вас нет уведомлений</h3>}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    adminNotifications: state.admin.adminNotifications
});

const mapDispatchToProps = dispatch => ({
    fetchAdminNotifications: () => dispatch(fetchAdminNotifications()),
    changeAdminNotificationStatus: (id) => dispatch(changeAdminNotificationStatus(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminNotificationsList);


