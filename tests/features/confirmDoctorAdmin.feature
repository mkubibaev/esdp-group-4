# language: ru

Функция: Подтверждение аккаунта доктора админом

  @confirmDoctorAdmin
  Сценарий: Подтвердить врача
    Допустим я нахожусь на странице Вход
    Если я ввожу "admin@mail.ru" в поле "email"
    И я ввожу "123" в поле "password"
    И нажимаю на кнопку "Войти"
    И я вижу текст "Вы вошли успешно"
    И я нахожусь на странице Список врачебных аккаутов
    И я нажимаю на кнопку "detailed"
    И я нажимаю на кнопку "addDoctor"
