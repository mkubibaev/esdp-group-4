# language: ru

Функция: Заполнения анкеты клиники

  @fillClinic
  Сценарий: Заполнить анкету
    Допустим я нахожусь на странице Вход
    Если я ввожу "clinic@example-2.kg" в поле "email"
    И я ввожу "123" в поле "password"
    И нажимаю на кнопку "Войти"
    И я вижу текст "Вы вошли успешно"
    И я нахожусь на странице Анкета
    И я ввожу "Ananaika_kids" в поле "title"
    И я ввожу "Магазин для малышей" в "description"
    И я ввожу "Бишкек" в поле "address"
    И я ввожу "555555555" в поле "phone"
    И я нажимаю кнопку для отправки "saveClinicProfile"
