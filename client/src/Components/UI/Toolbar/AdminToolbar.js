import React from 'react';
import {
    Badge,
    Nav,
    Navbar,
    NavbarBrand, NavItem
} from "reactstrap";
import {Link, NavLink as RouterNavLink} from "react-router-dom";

import './AdminToolbar.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEnvelope} from "@fortawesome/free-solid-svg-icons";

import logo from '../../../assets/images/logo.png'

const AdminToolbar = (props) => {
    return (
        <Navbar className="navBar mb-5" dark expand="md">
            <NavbarBrand tag={RouterNavLink} to="/admin">
                <img className={'logo'} src={logo} alt="logo"/>
            </NavbarBrand>
            <Nav className="ml-auto">
                <NavItem className="AdminToolbar mr-2">Здравствуйте, админ! </NavItem>
                {props.websocketMessage ?
                    <Link className="AdminNotification" onClick={props.toggleWebsocket} to="/admin/notifications"><Badge
                        color="danger" pill> <FontAwesomeIcon
                        icon={faEnvelope}/> </Badge></Link>
                    : null}
                <NavItem className="mx-4" onClick={props.logout}>
                    <RouterNavLink to="/" className="AdminToolbarLink">Выйти</RouterNavLink></NavItem>
            </Nav>
        </Navbar>
    );
};

export default AdminToolbar;
