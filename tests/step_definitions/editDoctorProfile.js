const {I} = inject();
// Add in your custom step files


When('я нахожусь на странице Общие данные доктора', () => {
    I.amOnPage('cabinet/doctor_profile');
});

When('я нажимаю кнопку для редактирования данных {string}', (fieldName) => {
    I.click({xpath: `//button[@id='${fieldName}']`});
});

When('я нахожусь на странице Анкета', () => {
    I.amOnPage('cabinet/profile');
});

When('я ввожу {string} в {string}', (text, fieldName) => {
    I.fillField({xpath: `//input[@id='${fieldName}']`}, text)
});

When('я выбираю {string} в {string}', (text, fieldName) => {
    I.selectOption({xpath:`//select[@id='${fieldName}']`}, text);
});

When('я ввожу {string} в {string}', (text, fieldName) => {
    I.fillField({xpath:`//textarea[@id='${fieldName}']`} , text)
});

When('я нажимаю кнопку для отправки {string}', (fieldName) => {
    I.click({xpath: `//button[@id='${fieldName}']`});
});



