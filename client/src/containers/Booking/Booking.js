import React, {Component} from 'react';
import Calendar from "react-calendar";
import {createBooking, getTimesOfDay} from "../../store/actions/bookingActions";
import {connect} from "react-redux/es/alternate-renderers";
import {Button, Col, Form, Row} from "reactstrap";
import {NotificationManager} from "react-notifications";
import FormElement from "../../Components/UI/Form/FormElement";
import {fetchPatientProfile} from "../../store/actions/profilesActions";

import './Booking.css'

class Booking extends Component {

    state = {
        selectedDate: null,
        selectedTime: null,
        nowDate: new Date(),
        isSelectedDate: false,
        isSelectedTime: false,
        complaint: '',
        name: "",
        surname: ""
    };

    componentDidMount() {
        this.onClickedDay(this.state.nowDate)
    }

    formatDate = (date) => {
        const monthNames = [
            "Января", "Февраля", "Марта",
            "Апреля", "Мая", "Июня", "Июля",
            "Августа", "Сентября", "Октября",
            "Ноября", "Декабря"
        ];

        const day = date.getDate();
        const monthIndex = date.getMonth();

        return day + ' ' + monthNames[monthIndex]
    };

    onClickedDay = (value) => {
        const changeValue = value.toLocaleDateString('ru-Ru');
        this.props.getTimesOfDay(this.props.match.params.id, changeValue);
        this.setState({selectedDate: value, isSelectedDate: true})

    };

    onClickTime = (time) => {
        this.setState({selectedTime: time, isSelectedTime: true})
    };

    createBooking = () => {
        const {selectedDate, selectedTime, complaint} = this.state;
        if (this.state.complaint === "") {
            NotificationManager.warning("Введите суть обращения", 5000);
        } else {
            const changeValue = selectedDate.toLocaleDateString('ru-Ru');
            let patient;
            let profile;
            if (this.props.patientProfile) {
                patient = this.props.patientProfile._id;
                profile = "patient";
            }
            if (this.props.patientForRecord !== "" && this.props.doctorProfile) {
                patient = this.props.patientForRecord._id;
                profile = "doctor";
            }
            const data = {
                doctorId: this.props.match.params.id,
                date: changeValue,
                time: selectedTime,
                complaint: complaint,
                patientId: patient,
                initiated: profile
            };
            this.props.createBooking(data);
            this.props.websocket.send(JSON.stringify({
                type: "CREATE_MESSAGE",
                profile: profile,
                patientId: patient,
                doctorId: this.props.match.params.id
            }))
        }
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    getFieldHasError = fieldName => {
        return (
            this.props.error &&
            this.props.error.errors &&
            this.props.error.errors[fieldName] &&
            this.props.error.errors[fieldName].message
        );
    };

    submitFormHandler = event => {
        event.preventDefault();
        if (this.state.name !== "") {
            this.props.fetchPatientForRecord(this.state.name);
        }
        if (this.state.surname !== "") {
            this.props.fetchPatientForRecord(this.state.surname);
        }
        if (this.state.name !== "" && this.state.surname !== "") {
            this.props.fetchPatientForRecord(this.state.name, this.state.surname);
        }
        this.setState({
            name: "",
            surname: ""
        })
    };

    dateFormat = date => {
        let d = new Date(date);
        return d.toLocaleDateString('ru-GB');
    };

    render() {
        const {bookingTime} = this.props;
        const {selectedDate, isSelectedDate, isSelectedTime, selectedTime} = this.state;

        return (
            <div className={'doctor_record_root'}>

                <h1 className={'doctor_record_root_title'}>Выберите дату и время</h1>

                <Row>
                    {this.props.doctorProfile ?
                        <Col xs="12">
                            <Form onSubmit={this.submitFormHandler}>
                                <Row>
                                    <Col xs='12' md='6'>
                                        <FormElement
                                            propertyName="name"
                                            title="Введите имя пациента:"
                                            type="text"
                                            value={this.state.name}
                                            onChange={this.inputChangeHandler}
                                            error={this.getFieldHasError('name')}
                                        />
                                    </Col>
                                    <Col xs='12' md='6'>
                                        <FormElement
                                            propertyName="surname"
                                            title="Введите фамилию пациента:"
                                            type="text"
                                            value={this.state.surname}
                                            onChange={this.inputChangeHandler}
                                            error={this.getFieldHasError('surname')}
                                        />
                                        <Button type="submit" color="primary">Найти</Button>
                                    </Col>
                                </Row>
                            </Form>
                            {this.props.patientForRecord !== "" ?
                                <div className="Patient">
                                    <p>Найден
                                        пациент: {this.props.patientForRecord.name} {this.props.patientForRecord.surname} {this.props.patientForRecord.thirdname}</p>
                                    <p>Дата рождения: {this.dateFormat(this.props.patientForRecord.dateOfBirth)}</p>
                                </div> : ""}
                        </Col>
                        : null}
                    <Col xs="12" md="6">
                        <div className={'doctor_record_datepicker'}>
                            <Calendar
                                value={this.state.nowDate}
                                onActiveDateChange={() => null}
                                minDate={new Date()}
                                onClickDay={this.onClickedDay}
                            />
                            <h4 className={'doctor_record_root_date_title'}>
                                {
                                    selectedDate
                                        ?
                                        this.formatDate(new Date(selectedDate))
                                        :
                                        this.formatDate(new Date())
                                }
                                <span className={'doctor_record_root_date_subTitle'}>
                            {selectedTime && selectedTime}
                        </span>
                            </h4>
                            <div>
                                {
                                    isSelectedDate &&
                                    bookingTime &&
                                    bookingTime.map((time, key) => (
                                        <button
                                            key={key}
                                            onClick={() => this.onClickTime(time)}
                                            className={'Booking_time_button'}
                                            type='button'
                                        >
                                            {time}
                                        </button>
                                    ))
                                }
                            </div>
                        </div>

                    </Col>
                    <Col xs={12} md="6">
                        <div className={''}>
                            <div className={'Booking_textArea'}>
                        <textarea placeholder="Введите причину Вашего обращения?"
                                  rows="20"
                                  name="complaint"
                                  value={this.state.complaint}
                                  id="complaint"
                                  cols="40"
                                  className="ui-autocomplete-input"
                                  autoComplete="off"
                                  aria-autocomplete="list"
                            // aria-haspopup="true"
                                  onChange={this.inputChangeHandler}/>
                            </div>

                            <div className={'Booking_send_button_root'}>
                                {isSelectedTime &&
                                <Button
                                    color='success'
                                    className={'Booking_send_button'}
                                    onClick={this.createBooking}>
                                    Отправить
                                </Button>
                                }
                            </div>
                        </div>
                    </Col>

                </Row>
            </div>

        );
    }
}

const mapStateToProps = state => ({
    bookingTime: state.bookingReducer.bookingTime,
    websocket: state.users.websocket,
    user: state.users.user,
    patientProfile: state.users.patientProfile,
    patientForRecord: state.profiles.patientForRecord,
    doctorProfile: state.users.doctorProfile
});

const mapDispatchToProps = dispatch => ({
    createBooking: data => dispatch(createBooking(data)),
    getTimesOfDay: (id, date) => dispatch(getTimesOfDay(id, date)),
    fetchPatientForRecord: (name, surname) => dispatch(fetchPatientProfile(name, surname))
});

export default connect(mapStateToProps, mapDispatchToProps)(Booking);