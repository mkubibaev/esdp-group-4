import React from 'react';
import {
    Nav,
    Navbar,
    NavbarBrand, NavItem, NavLink
} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import UserMenu from "./Menus/UserMenu";
import AnonimousMenu from "./Menus/AnonimousMenu";
import './Toolbar.css';
import logo from '../../../assets/images/logo.png'

 const Toolbar = (props) => {
     return (
        <Navbar className="navBar" dark expand="md">
            <NavbarBrand tag={RouterNavLink} to="/">
                <img className={'logo'} src={logo} alt="logo"/>
            </NavbarBrand>

            <Nav className="nav-toolbar" navbar>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/news" exact>Новости</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/clinicProfile" exact>Клиники</NavLink>
                </NavItem>
                {props.user ? <UserMenu logout={props.logout} websocketMessage={props.websocketMessage} toggleWebsocket={props.toggleWebsocket} /> : <AnonimousMenu/>}
            </Nav>
        </Navbar>
    );
};

export default Toolbar;
