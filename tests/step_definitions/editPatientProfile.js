const {I} = inject();
// Add in your custom step files


When('я нахожусь на странице Общие данные пациента', () => {
    I.amOnPage('cabinet/patient_profile');
});

When('я нажимаю кнопку для редактирования данных {string}', (fieldName) => {
    I.click({xpath: `//button[@id='${fieldName}']`});
});

When('я ввожу {string} в {string}', (text, fieldName) => {
    I.fillField({xpath: `//input[@id='${fieldName}']`}, text)
});

When('я выбираю {string} в {string}', (text, fieldName) => {
    I.selectOption({xpath:`//select[@id='${fieldName}']`}, text);
});

When('я нажимаю кнопку для отправки {string}', (fieldName) => {
    I.click({xpath: `//button[@id='${fieldName}']`});
});

Then('я вижу текст ответа от сервера {string}', text => {
    I.waitForText(text);
});



