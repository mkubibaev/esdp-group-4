import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchClinicsDoctors, infoClinic} from "../../store/actions/profilesActions";
import ClinicListItems from "../ClinicListItems/ClinicListItems";
import {Card, CardBody, Col, NavLink, Row} from "reactstrap";
import Thumbnail from "../Thumbnail/Thumbnail";

import {NavLink as RouterNavLink} from "react-router-dom";
import './infoClinic.css';

class InfoClinic extends Component {
    componentDidMount() {
        this.props.infoClinic(this.props.match.params.id);
        this.props.fetchClinicsDoctors(this.props.match.params.id);
    }

    render() {
        return (
            this.props.clinic ? <div>
                <h3>Информация о клинике</h3>
                <Row>
                    <Col xs="12">
                        <ClinicListItems
                            title={this.props.clinic.title}
                            description={this.props.clinic.description}
                            address={this.props.clinic.address}
                            phone={this.props.clinic.phone}
                        >
                            <div className="infoClinic">
                            <Thumbnail image={this.props.clinic && this.props.clinic.user && this.props.clinic.user.image}/>
                            </div>
                            </ClinicListItems>
                        {this.props.doctors.map(doctor => (
                            <Card key={doctor._id} className="my-2">
                                <CardBody className="infoClinic">
                                    {doctor.user.image ? <Thumbnail
                                        image={doctor.user.image}/> : null}
                                    <NavLink tag={RouterNavLink}
                                             to={'/doctors/' + doctor._id}
                                             exact
                                    >
                                        <h4>{doctor.name} {doctor.surname} {doctor.thirdname}</h4>
                                    </NavLink>
                                    <p>Специализация: {doctor.speciality}</p>
                                    <p>Место работы: {doctor.workAddress}</p>
                                    <p>Цена: {doctor.price} сом</p>
                                    <p>Номер телефона: {doctor.phone}</p>
                                </CardBody>
                            </Card>
                        ))}
                    </Col>
                </Row>
            </div> : null
        );
    }
}

const mapStateToProps = state => ({
    clinic: state.profiles.infoClinic,
    doctors: state.profiles.clinicDoctors
});

const mapDispatchToProps = dispatch => ({
    infoClinic: id => dispatch(infoClinic(id)),
    fetchClinicsDoctors: id => dispatch(fetchClinicsDoctors(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(InfoClinic);