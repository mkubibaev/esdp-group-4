import React, {Component} from 'react';
import {ClipLoader} from 'react-spinners';
import {connect} from "react-redux";

import {fetchCardInfo} from "../../store/actions/medicalCards";
import {Button, Col, Form, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import UserCabinetMenu from "../../Components/UserCabinetMenu/UserCabinetMenu";
import {Link} from "react-router-dom";
import Rating from "react-rating";
import FormElement from "../../Components/UI/Form/FormElement";
import {sendRating} from "../../store/actions/profilesActions";

class MedicalCardPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            anonymous: false,
            rating: "",
            review: ""
        };
        this.toggle = this.toggle.bind(this);
    }

    setAnonymous = () => {
        this.setState({anonymous: !this.state.anonymous});
    };

    toggle = () => {
        this.setState(prevState => ({
            modal: !prevState.modal,
        }));
    };

    inputChangeHandler = event => {
        this.setState({[event.target.name]: event.target.value})
    };

    submitChangeHandler = event => {
        event.preventDefault();
        this.setState({review: "", rating: 0});
        let rating = {
            patient: this.props.patientProfile._id,
            name: this.props.patientProfile.name,
            surname: this.props.patientProfile.surname,
            rating: this.state.rating,
            review: this.state.review,
            anonymous: this.state.anonymous,
            medicalCard: this.props.cardInfo._id,
            doctor: this.props.cardInfo.doctorId._id

        };
        this.props.sendRating(this.props.cardInfo.doctorId._id, rating);
        this.toggle();
    };

    makeRating = value => {
        this.setState({rating: value});
    };

    componentDidMount() {
        this.props.fetchCardInfo(this.props.match.params.id);
    }

    render() {
        return (
            <React.Fragment>
                <Row>
                    <Col xs="12" md="3">
                        <UserCabinetMenu/>
                    </Col>
                    <Col xs="12" md="9">
                        {!this.props.cardInfo ?
                            <div className={'cardPage_loader'}>
                                <ClipLoader
                                    sizeUnit={"px"}
                                    size={60}
                                    margin={'2px'}
                                    color={'#07019b'}
                                    loading={true}
                                />
                            </div>
                            :
                            <div className={'cardPage_info-block'}>
                                <h3>Диагноз: </h3>
                                <p>{this.props.cardInfo.diagnosis}</p>
                                <h3>Перенесенные и сопутствующие заболевания: </h3>
                                <p>{this.props.cardInfo.pastIllnesses ? this.props.cardInfo.pastIllnesses : '-'}</p>
                                <h3>Развитие настоящего заболевания: </h3>
                                <p>{this.props.cardInfo.presentDisease ? this.props.cardInfo.presentDisease : '-'}</p>
                                <h3>Лечащий врач</h3>
                                <div><Link
                                    to={"/doctors/" + this.props.cardInfo.doctorId._id}>{this.props.cardInfo.doctorId ? this.props.cardInfo.doctorId.name + " " + this.props.cardInfo.doctorId.surname : '-'}</Link>
                                </div>
                                <Button className="my-3" color = "primary" onClick={() => this.toggle()}>Оценить работу врача</Button>
                            </div>
                        }
                    </Col>
                </Row>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Оцените работчу врача</ModalHeader>
                    <ModalBody>
                        <Form>
                            <h3>Оцените работу врача по шкале от 1 до 10</h3>
                            <Rating
                                stop={10} onClick={value => this.makeRating(value)}
                                placeholderRating={this.state.rating}
                            />
                            <h3>Оставьте свой отзыв</h3>
                            <FormElement
                                propertyName="review"
                                type="text"
                                value={this.state.review}
                                onChange={this.inputChangeHandler}
                            />
                            <FormElement className="check-input"
                                         propertyName="anonymous"
                                         type="checkbox"
                                         title="Оставить отзыв анонимно"
                                         value={this.state.anonymous}
                                         onChange={this.setAnonymous}
                            />
                            <Button color='success' onClick={this.submitChangeHandler}>Отправить
                                отзыв и оценку</Button>
                        </Form>
                    </ModalBody>
                </Modal>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    cardInfo: state.medicalCards.cardInfo,
    patientProfile: state.users.patientProfile
});

const mapDispatchToProps = (dispatch) => ({
    fetchCardInfo: id => dispatch(fetchCardInfo(id)),
    sendRating: (id, data) => dispatch(sendRating(id, data))
});

export default connect(mapStateToProps, mapDispatchToProps)(MedicalCardPage);