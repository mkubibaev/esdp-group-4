import React, {Component} from 'react';
import {Button, Form, Row, Col} from "reactstrap";
import {connect} from "react-redux";
import FormElement from "../../Components/UI/Form/FormElement";
import {addPhoto} from "../../store/actions/usersActions";


class UserImageAddForm extends Component {
    state = {
        image: null
    };


    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };


    getFieldHasError = fieldName => {
        return (
            this.props.error &&
            this.props.error.errors &&
            this.props.error.errors[fieldName] &&
            this.props.error.errors[fieldName].message
        );
    };

    submitFormHandler = event => {
        event.preventDefault();
        const formData = new FormData();

        formData.append("image", this.state.image);


        this.props.addPhoto(formData);
    };


    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <Row>
                    <Col xs="12" md="6">
                        <FormElement
                            propertyName="image"
                            title="Прикрепите ваше фото:"
                            type="file"
                            onChange={this.fileChangeHandler}
                            error={this.getFieldHasError('image')}
                        />
                    </Col>
                </Row>
                <Button type="submit" color="success" onClick={this.props.onClick}>Сохранить</Button>
            </Form>
        );
    }
}

const mapStateToProps = state => ({
    error: state.users.error
});

const mapDispatchToProps = dispatch => ({
    addPhoto: data => dispatch(addPhoto(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserImageAddForm);
