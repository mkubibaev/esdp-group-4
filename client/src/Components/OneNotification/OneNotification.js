import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Card, CardBody, Col, Row} from "reactstrap";
import {fetchOneNotifications} from "../../store/actions/notificationsActions";
import {Link} from "react-router-dom";

class OneNotification extends Component {

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.fetchOneNotification(id);
    }
    
    render() {
        return (
            <Fragment>
                {this.props.oneNotification ?
                    <Row style={{margin: '20px'}}>
                        <Col sm='12'>
                            <Card>
                                <CardBody>
                                    <h5>{this.props.oneNotification.notification}</h5>
                                    <p>Дата визита: {this.props.oneNotification.record.date}</p>
                                    <p>Время визита: {this.props.oneNotification.record.time}</p>
                                    {this.props.user.profile.codeName === "patient" ?
                                        <Fragment>
                                            <p>Ваш врач - {this.props.oneNotification.record.doctorId.surname} {this.props.oneNotification.record.doctorId.name}</p>
                                            <Link to="/records/patient">Подробнее</Link>
                                        </Fragment>
                                        :
                                        <Fragment>
                                            <p>Ваш пациент - {this.props.oneNotification.record.patientId.surname} {this.props.oneNotification.record.patientId.name}</p>
                                            <i>Свои записи по датам можете проверить<Link to="/records/doctor"> здесь</Link></i>
                                        </Fragment>}
                                </CardBody>
                            </Card>
                        </Col>
                    </Row> : null}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    oneNotification: state.notification.oneNotification
});

const mapDispatchToProps = dispatch => ({
    fetchOneNotification: id => dispatch(fetchOneNotifications(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(OneNotification);
