const express = require('express');
const axios = require('axios');
const auth = require('../middleware/auth');
const Record = require('../models/Record');
const PatientProfile = require('../models/PatientProfile');
const config = require('../config');

const router = express.Router();

router.get('/', auth, async (req, res) => {
    try {
        let criteria = {};
        if (req.query.patientId) {
            criteria = {
                patientId: req.query.patientId
            };
        }
        if (req.query.doctorId && req.query.date) {
            criteria = {
                doctorId: req.query.doctorId,
                date: req.query.date
            };
        }
        const records = await Record.find(criteria).sort({date: 1}).populate({
            path: "doctorId patientId",
            select: {name: 'name', surname: 'surname', thirdname: 'thirdname', workAddress: "workAddress"}
        });
        return res.send(records);

    } catch (e) {
        return res.status(500).send(e);
    }
});

router.get('/:id', auth, async (req, res) => {
    const times = [
        '09:00',
        '09:30',
        '10:00',
        '10:30',
        '11:00',
        '11:30',
        '13:00',
        '13:30',
        '14:00',
        '14:30',
        '15:00',
        '15:30',
        '16:00',
        '16:30',
        '17:00',
        '17:30'
    ];
    try {
        const date = req.query.datetime;
        const doctorRecords = await Record.find({doctorId: req.params.id, date: date}).select('time');
        doctorRecords.map(item => {
            times.map(time => {
                if (item.time === time) {
                    const index = times.findIndex(ndx => ndx === time);
                    times.splice(index, 1)
                }
            })
        });
        return res.send(times)
    } catch (e) {
        return res.status(500).send(e)
    }
});

router.put('/:id', auth, async (req, res) => {
    try {
        const record = await Record.findById(req.params.id);
        let notificationData;
        let recordStatus;
        if (!record) {
            return res.sendStatus(404);
        }
        if (req.body.date) {
            record.date = req.body.date;
        }
        if (req.body.time) {
            record.time = req.body.time;
        }
        record.patientConfirm = req.body.patientConfirm;
        record.doctorConfirm = req.body.doctorConfirm;
        record.status = req.body.status;
        if (record.status === "approved") {
            recordStatus = "подтверждена"
        }
        if (record.status === "declined") {
            recordStatus = "отклонена"
        }
        if (record.status === "rescheduled") {
            recordStatus = "перенесена"
        }

        if (!req.body.patientConfirm) {
            notificationData = {
                notification: "Ваша запись " + recordStatus,
                doctor: record.doctorId,
                record: record._id
            };
        }
        if (!req.body.doctorConfirm) {
            notificationData = {
                notification: "Ваша запись " + recordStatus,
                patient: record.patientId,
                record: record._id
            };
        }
        if (!req.body.doctorConfirm && !req.body.patientConfirm) {
            notificationData = {
                notification: "Ваша запись " + recordStatus,
                patient: record.patientId,
                doctor: record.doctorId,
                record: record._id
            };
        }
        if (req.body.doctorConfirm) {
            notificationData = {
                notification: "Ваша запись " + recordStatus,
                patient: record.patientId,
                record: record._id
            };
        }
        if (req.body.patientConfirm) {
            notificationData = {
                notification: "Ваша запись " + recordStatus,
                doctor: record.doctorId,
                record: record._id
            };
        }
        await axios.post(config.wsURL + '/notifications', notificationData);

        await record.save();
        res.send({message: 'Запись в календаре обновлена'});
    } catch (e) {
        res.status(500).send(e)
    }
});

router.post('/', auth, async (req, res) => {
    try {
        let recordData;
        let data;
        if (req.body.initiated === "patient") {
            recordData = {
                date: req.body.date,
                time: req.body.time,
                patientConfirm: true,
                doctorId: req.body.doctorId,
                patientId: req.body.patientId,
                complaint: req.body.complaint
            };
            data = {
                notification: "В календаре создана новая запись",
                doctor: req.body.doctorId,
            };
        } else {
            recordData = {
                date: req.body.date,
                time: req.body.time,
                doctorConfirm: true,
                doctorId: req.body.doctorId,
                patientId: req.body.patientId,
                complaint: req.body.complaint
            };
            data = {
                notification: "В календаре создана новая запись",
                patient: req.body.patientId,
            };
        }
        const record = await new Record(recordData);
        await record.save();
        data.record = record._id;
        await axios.post(config.wsURL + '/notifications', data);
        return res.send({record, message: 'Запись отправлена на утверждение'});
    } catch
        (e) {
        return res.status(400).send(e);
    }
})
;

module.exports = router;