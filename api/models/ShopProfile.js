const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ShopProfileSchema = new Schema({
    title: {
        type: String,
        required: 'Введите название магазина'
    },
    description: {
        type: String,
        required: 'Введите описание магазина'
    },
    address: {
        type: String,
        required: 'Укажите адрес магазина'
    },
    phone: {
        type: String,
        required: 'Введите номер телефона'
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        unique: true
    }
});


const ShopProfile = mongoose.model('ShopProfile', ShopProfileSchema);

module.exports = ShopProfile;
