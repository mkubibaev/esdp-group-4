import {push} from 'connected-react-router';
import {NotificationManager} from "react-notifications";
import axios from '../../axios';
import {deleteRecordsAfterLogout} from "./recordsActions";
import {
    deleteDataAfterLogout,
    fetchClinicProfile,
    fetchDoctorProfile,
    fetchPatientProfile,
    fetchShopProfile
} from "./profilesActions";
import {deleteNotificationsAfterLogout} from "./notificationsActions";


export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const ADD_PHOTO_SUCCESS = 'ADD_PHOTO_SUCCESS';

export const LOGOUT_USER = 'LOGOUT_USER';

export const SAVE_WEBSOCKET = "SAVE_WEBSOCKET";
const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, user});
const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});

const addPhotoSuccess = image => ({type: ADD_PHOTO_SUCCESS, image});

export const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
export const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});
export const saveWebsocket = id => ({type: SAVE_WEBSOCKET, id});

export const logoutUser = () => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {'Authorization': token}};

        return axios.delete('/users/sessions', config).then(
            (response) => {
                dispatch({type: LOGOUT_USER});
                dispatch(deleteRecordsAfterLogout());
                dispatch(deleteNotificationsAfterLogout());
                dispatch(deleteDataAfterLogout());
                NotificationManager.success(response.data.message);
                dispatch(push('/'));
            },
            error => {
                NotificationManager.error('Не удалось выйти!');
            }
        )
    }
};

export const loginUser = userData => {
    return dispatch => {
        return axios.post('users/sessions', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                switch (response.data.user.profile.codeName) {
                    case 'doctor':
                        dispatch(fetchDoctorProfile());
                        break;
                    case 'patient':
                        dispatch(fetchPatientProfile());
                        break;
                    case 'shop':
                        dispatch(fetchShopProfile());
                        break;
                    case 'clinic':
                        dispatch(fetchClinicProfile());
                        break;
                    default:
                        break;
                }
                NotificationManager.success(response.data.message);
                if (response.data.user.role === "admin") {
                    dispatch(push('/admin'));
                } else {
                    dispatch(push('/'));
                }
            },
            error => {
                dispatch(loginUserFailure(error.response.data));
            }
        )
    }
};

export const registerUser = userData => {
    return dispatch => {
        return axios.post('/users', userData).then(
            response => {
                dispatch(registerUserSuccess(response.data.user));
                NotificationManager.success(response.data.message);
                dispatch(push('/'));
            },
            error => {
                if (error.response) {
                    dispatch(registerUserFailure(error.response.data))
                } else {
                    dispatch(registerUserFailure({global: 'Нет соединения!'}))
                }
            }
        )
    }
};


export const activateUser = token => {
    return dispatch => {
        return axios.put('/users/verify/' + token).then(
            (response) => {
                dispatch(registerUserSuccess(token));
                NotificationManager.success(response.data.message);
            }
        )
    }
};

export const addPhoto = data => {
    return dispatch => {
        return axios.put('/users', data).then(
            (response) => {
                dispatch(addPhotoSuccess(response.data.user.image));
                NotificationManager.success(response.data.message);
                dispatch(push('/cabinet'));
            }
        )
    }
};

export const editPassword = data => {
    return dispatch => {
        return axios.put('/users', data).then(
            (response) => {
                NotificationManager.success(response.data.message);
                dispatch(push('/cabinet'));
            },
                error => {
                    dispatch(registerUserFailure(error));
                    NotificationManager.error(error.response.data.error);
                }
        )
    }
};


