import React, {Component, Fragment} from 'react';
import {Button, Table} from "reactstrap";
import {connect} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleDoubleRight} from "@fortawesome/free-solid-svg-icons";

import {fetchClinicsAdmin} from "../../store/actions/adminActions";

class AdminClinicsList extends Component {

    componentDidMount() {
        this.props.fetchClinicsAdmin();
    }


    goToProfile = (route, id) => {
        this.props.history.push({
            pathname: '/admin/' + route + '/' + id
        })
    };

    render() {
        return (
            <Fragment>

                <h2 className="my-3">Клиники</h2>
                <div className="table-container">
                    <Table bordered>
                        <thead>
                        <tr>
                            <th>Номер</th>
                            <th>Имя</th>
                            <th>Статус подтверждения</th>
                            <th>Действие</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.props.clinics.map((clinic, index) => {
                            let status;
                            switch (clinic.approved) {
                                case 'approved':
                                    status = "подтвержден";
                                    break;
                                case 'declined':
                                    status = "отклонен";
                                    break;
                                case 'pending':
                                    status = "в ожидании";
                                    break;
                                default:
                                    break;
                            }

                            return <tr key={clinic._id}>
                                <td>{index + 1}</td>
                                <td>{clinic.title}</td>
                                <td>{status}</td>
                                <td>
                                    <Button id="detailedClinic" type="button" color="warning"
                                            onClick={() => this.goToProfile('clinic', clinic._id)}>
                                        Подробнее <FontAwesomeIcon icon={faAngleDoubleRight}/></Button>
                                </td>
                            </tr>
                        })}
                        </tbody>
                    </Table>
                </div>

            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.admin.error,
    clinics: state.admin.clinics
});

const mapDispatchToProps = dispatch => ({
    fetchClinicsAdmin: () => dispatch(fetchClinicsAdmin())
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminClinicsList);