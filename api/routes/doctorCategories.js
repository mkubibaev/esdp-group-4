const express = require('express');

const DoctorCategory = require('../models/DoctorCategory');


const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const categories = await DoctorCategory.find({isDeleted: false});
        return res.send(categories);
    } catch {
        return res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const DoctorCategory = await DoctorCategory.findById(req.params.id);

        return res.send(DoctorCategory);
    } catch {
        return res.sendStatus(500);
    }
});


module.exports = router;