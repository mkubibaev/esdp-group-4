import React from 'react'

import {shallow} from 'enzyme';
import Toolbar from '../../Components/UI/Toolbar/Toolbar';
import {MemoryRouter} from "react-router";

describe('Toolbar', () => {
    it('should render correctly', () => {
        const component = shallow(<MemoryRouter><Toolbar/></MemoryRouter>)
        const actual = component.html().includes('nav')
        expect(actual).toBeTruthy()
    });

    it('should render with props user', () => {
        const user = {name: 'Aleshka'}
        const component = shallow(<MemoryRouter><Toolbar user={user}/></MemoryRouter>)
        const actual = component.html().includes('nav')
        expect(actual).toBeTruthy()
    });

    it('should render with props logout', () => {
        const component = shallow(<MemoryRouter><Toolbar logout={() => null}/></MemoryRouter>)
        const actual = component.html().includes('nav')
        expect(actual).toBeTruthy()
    });

})