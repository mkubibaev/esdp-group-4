const {I} = inject();
// Add in your custom step files

When('я нахожусь на странице Список врачебных аккаутов', () => {
    I.amOnPage('admin/doctors');
});

When('я нажимаю на кнопку {string}', (fieldName) => {
    I.click({xpath:`//button[@id='${fieldName}']`});
});
