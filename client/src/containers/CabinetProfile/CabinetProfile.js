import React, {Component} from 'react';
import {connect} from "react-redux";

import DoctorProfileAdd from "../DoctorProfileAdd/DoctorProfileAdd";
import DoctorProfileEdit from "../DoctorProfileEdit/DoctorProfileEdit";
import PatientProfileAdd from "../PatientProfileAdd/PatientProfileAdd";
import PatientProfileEdit from "../PatientProfileEdit/PatientProfileEdit";
import ClinicProfileAdd from "../ClinicProfileAdd/ClinicProfileAdd";
import ClinicProfileEdit from "../ClinicProfileEdit/ClinicProfileEdit";
import ShopProfileAdd from "../ShopProfileAdd/ShopProfileAdd";
import ShopProfileEdit from "../ShopProfileEdit/ShopProfileEdit";


class CabinetProfile extends Component {

    render() {
        let form;

        switch (this.props.user.profile.codeName) {
            case 'doctor':
                this.props.doctorProfile ? form = <DoctorProfileEdit/> : form = <DoctorProfileAdd/>;
                break;
            case 'shop':
                this.props.shopProfile ? form = <ShopProfileEdit/> : form = <ShopProfileAdd/>;
                break;
            case 'clinic':
                this.props.clinicProfile ? form = <ClinicProfileEdit/> : form = <ClinicProfileAdd/>;
                break;
            default:
                this.props.patientProfile ? form = <PatientProfileEdit/> : form = <PatientProfileAdd/>
        }
        return (
            <div className="py-3 mb-3">
                {form}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    error: state.profiles.error,
    patientProfile: state.users.patientProfile,
    doctorProfile: state.users.doctorProfile,
    clinicProfile: state.users.clinicProfile,
    shopProfile: state.users.shopProfile
});


export default connect(mapStateToProps)(CabinetProfile);
