
import {
    ADMIN_DELETE_FAILURE,
    ADMIN_FETCH_DOCTORS_SUCCESS,
    ADMIN_FETCH_DOCTORS_FAILURE,
    ADMIN_FETCH_CLINICS_SUCCESS,
    ADMIN_FETCH_CLINICS_FAILURE,
    ADMIN_TOGGLE_APPROVED_FAILURE,
    ADMIN_FETCH_CLINIC_ID_FAILURE,
    ADMIN_FETCH_DOCTOR_ID_FAILURE,
    ADMIN_FETCH_CLINIC_ID_SUCCESS,
    ADMIN_FETCH_DOCTOR_ID_SUCCESS,
    ADMIN_FETCH_DOCTOR_CATEGORIES_SUCCESS,
    ADMIN_FETCH_DOCTOR_CATEGORIES_FAILURE,
    ADMIN_FETCH_DOCTOR_CATEGORY_SUCCESS,
    ADMIN_FETCH_DOCTOR_CATEGORY_FAILURE,
    ADMIN_ADD_FAILURE,
    FETCH_ADMIN_NOTIFICATIONS_FAILURE,
    FETCH_ADMIN_NOTIFICATIONS_SUCCESS, ADMIN_TOGGLE_WEBSOCKET
} from "../actions/adminActions";

const initialState = {
    error: null,
    doctors: [],
    clinics: [],
    doctorId: [],
    clinicId: [],
    doctorCategories: [],
    doctorCategoryForEdit: [],
    adminNotifications: [],
    adminWebsocketMessage: false

};

const adminReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADMIN_TOGGLE_APPROVED_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case ADMIN_FETCH_DOCTORS_SUCCESS:
            return {
                ...state,
                doctors: action.data
            };
        case ADMIN_FETCH_DOCTORS_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case ADMIN_FETCH_CLINICS_SUCCESS:
            return {
                ...state,
                clinics: action.data
            };
        case ADMIN_FETCH_CLINICS_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case ADMIN_FETCH_DOCTOR_CATEGORIES_SUCCESS:
            return {
                ...state,
                doctorCategories: action.data
            };
        case ADMIN_FETCH_DOCTOR_CATEGORIES_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case ADMIN_FETCH_DOCTOR_CATEGORY_SUCCESS:
            return {
                ...state,
                doctorCategoryForEdit: action.data
            };
        case ADMIN_FETCH_DOCTOR_CATEGORY_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case ADMIN_FETCH_CLINIC_ID_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case ADMIN_FETCH_CLINIC_ID_SUCCESS:
            return {
                ...state,
                clinicId: action.data
            };
        case ADMIN_FETCH_DOCTOR_ID_SUCCESS:
            return {
                ...state,
                doctorId: action.data
            };
        case ADMIN_FETCH_DOCTOR_ID_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case ADMIN_DELETE_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case ADMIN_ADD_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case FETCH_ADMIN_NOTIFICATIONS_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case FETCH_ADMIN_NOTIFICATIONS_SUCCESS:
            return {
                ...state,
                adminNotifications: action.data
            };
        case ADMIN_TOGGLE_WEBSOCKET:
            return {
                ...state,
                adminWebsocketMessage: !state.adminWebsocketMessage
            };

        default:
            return state;
    }
};
export default adminReducer;