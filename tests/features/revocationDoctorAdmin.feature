# language: ru

Функция: Отмена аккаунта доктора админом

  @revocationDoctorAdmin
  Сценарий: Отклонить врача
    Допустим я нахожусь на странице Вход
    Если я ввожу "admin@mail.ru" в поле "email"
    И я ввожу "123" в поле "password"
    И нажимаю на кнопку "Войти"
    И я вижу текст "Вы вошли успешно"
    И я нахожусь на странице Список врачебных аккаутов
    И я нажимаю на кнопку "detailed"
    И я нажимаю на кнопку "declineDoctor"
    То статус записи изменяется на "Подтверждение отклонено"