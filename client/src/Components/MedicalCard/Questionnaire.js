import React, {Component} from 'react';
import {Button, Col, FormGroup, Input, Label, Row} from "reactstrap";
import {connect} from "react-redux";
import {createQuestionnaire, editQuestionnaire} from "../../store/actions/medicalCards";
import {NotificationManager} from "react-notifications";

import './MedicalCard.css'


class Questionnaire extends Component {

    state = {
        options: []
    }

    changeHandler = (e) => {
        const options = this.state.options
        let index

        if (e.target.checked) {
            options.push(e.target.name)
        } else {
            index = options.findIndex(ndx => ndx.name === e.target.name)
            options.splice(index, 1)
        }

        this.setState({options: options})
    };

    saveInformation = () => {
        if (this.state.options.length) {
            const data = {
                questionnaire: this.state.options
            }
            this.props.createQuestionnaire(this.props.match.params.id, data)
        } else NotificationManager.error('Заполните анкету!')
    };

    editInformation = () => {
        if (this.state.options.length) {
            const data = {
                questionnaire: this.state.options
            }
            this.props.editQuestionnaire(this.props.match.params.questId, data)
        } else NotificationManager.error('Заполните анкету!')
    }

    render() {
        return (
            <Row>
                <Col xs="12" md="12">
                    <h1 className={'medical-card-header-title'}>Анкета</h1>
                    <h5 className={'medical-card-header-title'}>Об общем состоянии здоровья</h5>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input type="checkbox"
                               id="1"
                               onChange={this.changeHandler}
                               name={'Находится под наблюдением'}
                        />
                        <Label check for="1">
                            Находитесь ли Вы на данный момент под наблюдением врача?
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="2"
                            onChange={this.changeHandler}
                            name={'Аллергические реакции'}
                        />
                        <Label check for="2">
                            Есть ли у Вас аллергические реакции?
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="8">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="3"
                            onChange={this.changeHandler}
                            name={'Принимает лекарства, в том числе гормональные'}
                        />
                        <Label check for="3">
                            Принимаете ли Вы какие-либо лекарства постоянно, в том числе гормональные?
                        </Label>
                    </FormGroup>
                </Col>
                <div>
                    <h3 style={{textAlign: 'left', paddingLeft: '15px', margin: '15px 0'}}>Отметьте, если есть
                        следующие заболевания: </h3>
                </div>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="4"
                            onChange={this.changeHandler}
                            name={'Сахарный диабет'}
                        />
                        <Label check for={'4'}>
                            Сахарный диабет
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="5"
                            onChange={this.changeHandler}
                            name={'Астма'}
                        />
                        <Label check for={'5'}>
                            Астма
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="7"
                            onChange={this.changeHandler}
                            name={'Сердечно-сосудистые заболевания'}
                        />
                        <Label check for={'7'}>
                            Сердечно-сосудистые заболевания
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="8"
                            onChange={this.changeHandler}
                            name={'Плохая свертываемость крови'}
                        />
                        <Label check for={'8'}>
                            Плохая свертываемость крови
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="9"
                            onChange={this.changeHandler}
                            name={'Аритмия'}
                        />
                        <Label check for={'9'}>
                            Аритмия
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="10"
                            onChange={this.changeHandler}
                            name={'Эпилепсия'}
                        />
                        <Label check for={'10'}>
                            Эпилепсия
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="11"
                            onChange={this.changeHandler}
                            name={'Наличие кардио-стимулятора'}
                        />
                        <Label check for={'11'}>
                            Наличие кардио-стимулятора
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="12"
                            onChange={this.changeHandler}
                            name={'Восполнение суставов (ревматизм)'}
                        />
                        <Label check for={'12'}>
                            Восполнение суставов (ревматизм)
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="13"
                            onChange={this.changeHandler}
                            name={'Онкологические заболевания'}
                        />
                        <Label check for={'13'}>
                            Онкологические заболевания
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="14"
                            onChange={this.changeHandler}
                            name={'Онкологические заболевания'}
                        />
                        <Label check for={'14'}>
                            Повышенное давление
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="15"
                            onChange={this.changeHandler}
                            name={'Пониженное давление'}
                        />
                        <Label check for={'15'}>
                            Пониженное давление
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="16"
                            onChange={this.changeHandler}
                            name={'Инфаркт миокарда'}
                        />
                        <Label check for={'16'}>
                            Инфракт миокарда
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="17"
                            onChange={this.changeHandler}
                            name={'Гепатит (желтуха)'}
                        />
                        <Label check for={'17'}>
                            Гепатит (желтуха)
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="18"
                            onChange={this.changeHandler}
                            name={'Курение'}
                        />
                        <Label check for={'18'}>
                            Курение
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="19"
                            onChange={this.changeHandler}
                            name={'Употребление наркотических средств'}
                        />
                        <Label check for={'19'}>
                            Употребляете ли наркотические средства?
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="20"
                            onChange={this.changeHandler}
                            name={'Алкоголь'}
                        />
                        <Label check for={'20'}>
                            Алкоголь
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="21"
                            onChange={this.changeHandler}
                            name={'Психические заболевания'}
                        />
                        <Label check for={'21'}>
                            Психические заболевания
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="22"
                            onChange={this.changeHandler}
                            name={'СПИД ВИЧ'}
                        />
                        <Label check for={'22'}>
                            СПИД ВИЧ
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="23"
                            onChange={this.changeHandler}
                            name={'Туберкулез'}
                        />
                        <Label check for={'23'}>
                            Туберкулез
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="24"
                            onChange={this.changeHandler}
                            name={'Переливание крови'}
                        />
                        <Label check for={'24'}>
                            Переливание крови
                        </Label>
                    </FormGroup>
                </Col>
                <Col xs="12" md="6">
                    <FormGroup check>
                        <Input
                            type="checkbox"
                            id="25"
                            onChange={this.changeHandler}
                            name={'Беременность'}
                        />
                        <Label check for={'25'}>
                            Есть ли беременность? Срок?
                        </Label>
                    </FormGroup>
                </Col>
                <Col sm="12" md={{size: 5, offset: 5}} className={'saveButton_quest'}>
                    {
                        this.props.match.params.questId
                            ? <Button
                                color="success"
                                onClick={this.editInformation}>
                                Изменить
                            </Button>
                            : <Button
                                color="success"
                                onClick={this.saveInformation}>
                                Сохранить
                            </Button>
                    }
                </Col>
            </Row>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createQuestionnaire: (id, data) => dispatch(createQuestionnaire(id, data)),
    editQuestionnaire: (id, data) => dispatch(editQuestionnaire(id, data))
});

export default connect(null, mapDispatchToProps)(Questionnaire);
