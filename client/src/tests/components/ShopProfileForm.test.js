import React from 'react'

import {shallow} from 'enzyme';
import ShopProfileForm from "../../Components/ShopProfileForm/ShopProfileForm";

describe('ShopProfileForm', () => {
    it('should render with props', () => {
        const wrapper = shallow(
            <ShopProfileForm
                error={'Some error message'}
                onSubmit={() => null}
            />);
        expect(wrapper).toHaveLength(1);
    });
})
