const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PatientProfileSchema = new Schema({
    name: {
        type: String,
        required: 'Введите имя'
    },
    surname: {
        type: String,
        required: 'Введите фамилию'
    },
    thirdname: {
        type: String,
        required: 'Введите отчество'
    },
    dateOfBirth: {
        type: String,
        required: 'Введите дату рождения'
    },
    gender: {
        type: String,
        required: 'Введите пол',
        enum: ['male', 'female']
    },
    passportId: {
        type: String,
        required: 'Введите айди пасспорта'
    },
    passportNumber: {
        type: Number,
        required: 'Введите номер паспорта'
    },
    phone: {
        type: String,
        required: 'Введите номер телефона'
    },
    workAddress: {
        type: String,
        required: 'Введите рабочий адрес'
    },
    residence: {
        type: String,
        required: 'Введите место жительства'
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        unique: true
    }
});


const PatientProfile = mongoose.model('PatientProfile', PatientProfileSchema);

module.exports = PatientProfile;
